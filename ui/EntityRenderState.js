import * as SIM from "/common/simulationConstants.js";
import * as GFX from "./gfx.js";

const sqrt2 = Math.sqrt(2);

// Responsible for keeping track of Entity animations during render frames and performing movement interpolation between game tick updates.
export default class EntityRenderState
{
	// Accepts an entity which this EntityRenderState animates for.
	constructor(ent)
	{
		this.parent = ent;
		this.init({ method: "instant" });
	}

	// Accepts a configurator object whose properties are assigned to this EntityRenderState.
	init(config)
	{
		Object.assign(this,
			{
				method: "lerp",
				duration: SIM.GAME_TICK_INTERVAL_MILLIS,
				origin: this.lastRender || { x: this.parent.x, y: this.parent.y },
				destination: this.parent, // Can be any Vector2-like object.
				interpolationRatio: 0,
				markedForDeletion: false,
				deleted: false
			},
			config);
	}

	/*
	 * Indicates that this Entity should be deleted once the animation is finished.
	 * If no animation is in progress, simply delays deletion for a game tick.
	 */
	markForDeletion()
	{
		if(this.method === "instant"){this.init();}
		this.markedForDeletion = true;
	}

	/*
	 * Indicates whether an animation is currently playing.
	 * Takes am optional parameter in millis. If the time remaining is less than that amount,
	 * the function will assume the animation is finished.
	 */
	isAnimationPlaying(threshhold)
	{
		const timeLeft = (1 - this.interpolationRatio) * this.duration;
		return this.method !== "instant" && (timeLeft > (threshhold || 0));
	}

	/*
	 * Should be called each render frame with a deltaTime in millis.
	 * Returns a Vector2-like object for where the Entity should be rendered this frame.
	 */
	tick(deltaTime)
	{
		if(this.interpolationRatio >= 1){this.method = "instant";}
		const { method, interpolationRatio: ratio, origin, destination } = this;
		this.interpolationRatio += deltaTime / this.duration;
		const ox = origin.x, oy = origin.y, dx = destination.x, dy = destination.y, xDiff = dx - ox, yDiff = dy - oy;
		switch(method)
		{
		case "lerp":
			return this.lastRender = {
				x: ox + xDiff * ratio,
				y: oy + yDiff * ratio
			};
		case "quadInOut":
		{
			const flip = ratio >= 0.5;
			const innerT = (flip ? 1 - ratio : ratio) * sqrt2;
			const mul = flip ? -1 : 1;
			return this.lastRender = {
				x: ( flip ? dx : ox ) + innerT * innerT * xDiff * mul,
				y: ( flip ? dy : oy ) + innerT * innerT * yDiff * mul
			};
		}
		case "instant":
		default:
			/*
			 * Instant is a special case which is considered the "end" of the animation.
			 * In addition to rendering at the Entity's true position, it also performs cleanup.
			 */
			if(this.markedForDeletion){this.deleted = true;}
			return this.lastRender = {x: dx, y: dy};
		}
	}

	// Returns the Entity's bounding box, accounting for the current state of the movement interpolation.
	getBoundingBox()
	{
		return this.parent.getBoundingBox(this.lastRender || this.parent);
	}

	/*
	 * Returns the bounding box in screenspace.
	 * The resulting bounds will encompass all points of the original bounding box,
	 * and thus the resulting area will be larger due to rotation of rectangles.
	 */
	getScreenSpaceBoundingBox()
	{
		const worldBox = this.getBoundingBox();
		const perimeterPoints =
		[
			GFX.transformWorldVectorToScreenVector({ x: worldBox.left, y: worldBox.top }),
			GFX.transformWorldVectorToScreenVector({ x: worldBox.right, y: worldBox.top }),
			GFX.transformWorldVectorToScreenVector({ x: worldBox.right, y: worldBox.bottom }),
			GFX.transformWorldVectorToScreenVector({ x: worldBox.left, y: worldBox.bottom })
		];
		const retVal = { left: perimeterPoints[0].x, top: perimeterPoints[0].y, right: perimeterPoints[0].x, bottom: perimeterPoints[0].y };
		for(let i = 1; i < perimeterPoints.length; i++)
		{
			retVal.left = Math.min(retVal.left, perimeterPoints[i].x);
			retVal.right = Math.max(retVal.right, perimeterPoints[i].x);
			retVal.top = Math.min(retVal.top, perimeterPoints[i].y);
			retVal.bottom = Math.max(retVal.bottom, perimeterPoints[i].y);
		}
		return retVal;
	}
}