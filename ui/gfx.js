import * as SIM from "/common/simulationConstants.js";
import * as LOG from "/common/logging.js";
import * as API from "./api.js";
import EntityRenderState from "./EntityRenderState.js";
import * as TileGrid from "/common/TileGrid.js";
import * as RenderQueue from "./renderQueue.js";
import * as MISC from "/common/misc.js";
import * as BG from "./backgroundUtils.js";

// Passing this constant as the renderPriority value for renderSprite will bypass the render queue and draw synchronously.
export const RENDER_PRIORITY_IMMEDIATE = "immediate";

const ENTITY_LAYER_DECORATOR_PRIORITY = -1;
const ENTITY_LAYER_DEFAULT_PRIORITY = 0;

const BACKGROUND_PARALLAX_FACTOR = 0.4;

// SPRITE_DEFINITIONS is a mapping of sprite names to metadata describing those sprites.
let SPRITE_DEFINITIONS;

/*
 * SPRITE_ASSETS is a mapping of filename to Image instances, to reduce memory footprint
 * to a single instance per image file.
 */
let SPRITE_ASSETS;

// VIEWPORT is the HTML element set aside for injection of the client's renderable surfaces.
const VIEWPORT = document.querySelector(".mainViewport");
if(!VIEWPORT)
{
	const reason = "Cannot find viewport. Something went terribly wrong.";
	LOG.error(reason);
	throw reason;
}
export const getViewportSize = () => ({ x: VIEWPORT.offsetWidth, y: VIEWPORT.offsetHeight });
export const registerEventListener = (...args) => VIEWPORT.addEventListener(...args);

let _cameraPosition = { x: 0, y: 0 };
// Returns the camera position by reference.
export const cameraPosition = () => _cameraPosition;

/*
 * KVS with all the possible transforms we might need.
 * Transform indexes are:
 * [ 0, 2, 4
 *   1, 3, 5 ]
 * context.transform takes a 3x2 matrix to account for constant-term offset, but for ease-of-use reasons,
 * the constant-term offset values at indices 4 and 5 are treated differently. For regular transformations,
 * they are applied as negative scalar values to the camera position and applied at the end. For inverse
 * transformations, they are applied as positive scalars at the beginning.
 */
const transformLibrary =
{
	"identity":
	{
		transform: [1, 0, 0, 1, 0, 0]
	},
	"isometric":
	{
		transform: [1, -0.5, 1, 0.5, 1, 1]
	},
	"isometric-global":
	{
		transform: [1, -0.5, 1, 0.5, 0, 0]
	}
};

/*
 * Generates an inverse transform using the same layout as the transforms in transformLibrary.
 * Camera offset values are passed through unmodified.
 */
export const createInverseTransform = (transform) =>
{
	const determinant = transform[0] * transform[3] - transform[2] * transform[1];
	return [transform[3] / determinant, transform[1] / -determinant, transform[2] / -determinant, transform[0] / determinant, transform[4], transform[5]];
};
for(const transformType in transformLibrary)
{
	const dat = transformLibrary[transformType];
	dat.inverse = createInverseTransform(dat.transform);
}

/*
 * Takes a 2d vector and a transform, and applies the transform to that vector, returning a new one.
 * Includes logic for the special-case camera offset vector, applying it at the end of the transformation.
 * Optionally takes a boolean that if true, inverts the camera offset logic and applies it before the
 * transformation. (This would be used for inverse transforms involving screenspace coordinates.)
 */
export const applyTransformToVector = (vec, transform, inverseCameraOffset) =>
{
	let retVal = {...vec};
	if(inverseCameraOffset)
	{
		retVal.x += _cameraPosition.x * transform[4];
		retVal.y += _cameraPosition.y * transform[5];
	}
	retVal = {
		x: retVal.x * transform[0] + retVal.y * transform[2],
		y: retVal.x * transform[1] + retVal.y * transform[3]
	};
	if(!inverseCameraOffset)
	{
		retVal.x -= _cameraPosition.x * transform[4];
		retVal.y -= _cameraPosition.y * transform[5];
	}
	return retVal;
};

// Takes a screenspace coordinate and returns a new vector in game world coordinates.
export const transformScreenVectorToWorldVector = (screenVec) =>
	applyTransformToVector(screenVec, transformLibrary.isometric.inverse, true);

// Takes a worldspace coordinate and returns a new vector cooresponding to the screen position.
export const transformWorldVectorToScreenVector = (worldPos) =>
	applyTransformToVector(worldPos, transformLibrary.isometric.transform);

export const transformLayerVectorToScreenVector = (layerProps, layerVec) =>
	applyTransformToVector(layerVec, transformLibrary[layerProps.defaultTransform].transform);

// Automatically generate some metadata common to all isometric layers.
const ISOMETRIC_CANVAS_POINTS =
[
	{x: 0, y: 0},
	{x: SIM.SIMULATION_MAX_SIZE.x, y: 0},
	SIM.SIMULATION_MAX_SIZE,
	{x: 0, y: SIM.SIMULATION_MAX_SIZE.y}
].map(p => applyTransformToVector(p, transformLibrary["isometric-global"].transform));
const ISOMETRIC_CANVAS_RECT =
{
	left: ISOMETRIC_CANVAS_POINTS.map(p => p.x).reduce((prev, next) => Math.min(prev, next)),
	top: ISOMETRIC_CANVAS_POINTS.map(p => p.y).reduce((prev, next) => Math.min(prev, next)) - (SIM.LEVEL_GENERATION_MAX_HEIGHT - 1) * SIM.GRID_SIZE,
	right: ISOMETRIC_CANVAS_POINTS.map(p => p.x).reduce((prev, next) => Math.max(prev, next)),
	bottom: ISOMETRIC_CANVAS_POINTS.map(p => p.y).reduce((prev, next) => Math.max(prev, next)) + SIM.GRID_SIZE
};
export const ISOMETRIC_CANVAS_ORIGIN =
{
	x: ISOMETRIC_CANVAS_POINTS[0].x - ISOMETRIC_CANVAS_RECT.left,
	y: ISOMETRIC_CANVAS_POINTS[0].y - ISOMETRIC_CANVAS_RECT.top
};
export const ISOMETRIC_CANVAS_SIZE =
{
	x: ISOMETRIC_CANVAS_RECT.right - ISOMETRIC_CANVAS_RECT.left,
	y: ISOMETRIC_CANVAS_RECT.bottom - ISOMETRIC_CANVAS_RECT.top
};

/*
 * There are multiple canvas elements representing different 2D layers that are overlaid on top of each other.
 * Each has slightly different behavior, but the main benefit is automatic z-ordering.
 */
export const BACKGROUND_LAYER = 0;
export const TERRAIN_LAYER = 1;
export const TILE_LAYER = 2;
export const VISION_LAYER = 3;
export const ENTITY_LAYER = 4;
export const HUD_LAYER = 5;
export const LAYER_PROPERTIES =
{
	[BACKGROUND_LAYER]:
	{
		// Sparse layers are not re-rendered completely each frame.
		sparse: true,
		// Layers not domRendered are considered off-screen render buffers.
		domRendered: true,
		// Default transform is the one applied to the canvas at the start of each frame, by name.
		defaultTransform: "isometric-global",
		// The 2D size of the canvas element, which defaults to the screen space dimensions.
		size: ISOMETRIC_CANVAS_SIZE,
		// The 2D offset for the origin point of drawing operations, which defaults to 0, 0.
		offset: ISOMETRIC_CANVAS_ORIGIN
	},
	[TERRAIN_LAYER]:
	{
		sparse: true,
		domRendered: true,
		defaultTransform: "isometric-global",
		size: ISOMETRIC_CANVAS_SIZE,
		offset: ISOMETRIC_CANVAS_ORIGIN
	},
	[TILE_LAYER]:
	{
		sparse: true,
		domRendered: true,
		defaultTransform: "isometric-global",
		size: ISOMETRIC_CANVAS_SIZE,
		offset: ISOMETRIC_CANVAS_ORIGIN
	},
	[VISION_LAYER]:
	{
		sparse: false,
		domRendered: true,
		defaultTransform: "isometric"
	},
	[ENTITY_LAYER]:
	{
		sparse: false,
		domRendered: true,
		defaultTransform: "isometric"
	},
	[HUD_LAYER]:
	{
		sparse: false,
		domRendered: true,
		defaultTransform: "identity"
	}
};
export const LAYER_COUNT = Object.keys(LAYER_PROPERTIES).length;
for(const layer in LAYER_PROPERTIES){LAYER_PROPERTIES[layer].id = parseInt(layer);}
export const getLayerProps = (layerId) => LAYER_PROPERTIES[layerId];
export const CANVAS = (layer) => LAYER_PROPERTIES[layer].canvas;
export const CONTEXT = (layer) => LAYER_PROPERTIES[layer].context;
export const getCanvasSize = (layer) =>
{
	let { size, canvas } = getLayerProps(layer);
	let { x, y } = size || { x: canvas.offsetWidth, y: canvas.offsetHeight };
	return { x, y };
};

/*
 * Given a matcher object, returns an iterable that iterates over each layer that matches the specified properties.
 * Providing no matcher results in iterating over all layers.
 */
export const layersMatching = (matcher) =>
	({
		[Symbol.iterator]: () =>
		{
			let i = 0;
			return {
				next: () =>
				{
					while(i < LAYER_COUNT)
					{
						const layerData = getLayerProps(i++);
						let match = true;
						for(const key in matcher || {})
						{
							if(matcher[key] !== layerData[key])
							{
								match = false;
								break;
							}
						}
						if(match)
						{
							return { done: false, value: layerData };
						}
					}
					return { done: true };
				}
			};
		}
	});

// Returns a promise that resolves when the graphics subsystem is initialized.
export const init = () =>
{
	for(const layer of layersMatching())
	{
		if(layer.canvas || layer.context)
		{
			LOG.trace("Forcefully unloading canvas:", layer.id);
			if(layer.domRendered){VIEWPORT.removeChild(layer.canvas);}
			layer.canvas = null;
			layer.context = null;
		}
	}
	LOG.trace("Initializing canvases.");
	const size = getViewportSize();
	for(const layer of layersMatching())
	{
		const canvas = layer.canvas = document.createElement("canvas");
		canvas.width = layer.size?.x || size.x;
		canvas.height = layer.size?.y || size.y;
		const context = layer.context = canvas.getContext("2d");
		context.imageSmoothingEnabled = false;
		if(layer.domRendered){VIEWPORT.appendChild(canvas);}
		switchRenderTransform(layer.id, layer.defaultTransform, layer.offset);
	}
	// Clientside code can't use the JSON import syntax, so this dependency is acquired via traditional AJAX call.
	SPRITE_ASSETS = {};
	return API.get("./SpriteDefinitions.json")
		.then(obj =>
		{
			SPRITE_DEFINITIONS = obj;
			LOG.trace("Loaded sprite definitions.");
			BG.renderBackground();
		})
		.catch(reason =>
		{
			LOG.error("Failed to acquire sprite definitions:", reason);
		});
};

/*
 * Changes which render transform is being applied to upcoming render operations.
 * Takes a 2D rendering context and a transform by name, and applies the transform
 * to that context.
 * The constant-term matrix indices specify scalar values for the camera position as normal.
 * Optionally takes a 2d offset which is applied at the end.
 */
export const switchRenderTransform = (layerName, transformType, offset) =>
{
	const props = getLayerProps(layerName);
	const { currentTransform, context } = props;
	if(currentTransform !== transformType)
	{
		props.currentTransform = transformType;
		const transform = [...transformLibrary[transformType].transform];
		transform[4] *= -_cameraPosition.x;
		transform[5] *= -_cameraPosition.y;
		transform[4] += offset?.x || 0;
		transform[5] += offset?.y || 0;
		context.setTransform(...transform);
	}
};

// Resets the render space in preparation for a new render frame.
let frameStartTime;
export const beginFrame = () =>
{
	frameStartTime = performance.now();
	for(const layer of layersMatching())
	{
		const ctx = layer.context;
		if(!layer.sparse)
		{
			// Easiest way to clear the canvas to fully transparent is by abusing this language quirk.
			const { x: width } = getCanvasSize(layer.id);
			layer.canvas.width = width;
			layer.currentTransform = null;
		}
		ctx.font = "18px Arial";
		ctx.lineWidth = 4;
		ctx.strokeStyle = "#FFFFFF";
		ctx.fillStyle = "#FFFFFF";
		ctx.globalAlpha = 1;
	}
	const visionLayer = getLayerProps(VISION_LAYER);
	const visionCtx = visionLayer.context;
	visionCtx.globalAlpha = 0.5;
	visionCtx.fillStyle = "#000000";
	switchRenderTransform(VISION_LAYER, visionLayer.defaultTransform);
	visionCtx.fillRect(
		-SIM.GRID_SIZE,
		-SIM.GRID_SIZE * (SIM.LEVEL_GENERATION_MAX_HEIGHT - 1),
		SIM.SIMULATION_MAX_SIZE.x + SIM.GRID_SIZE * SIM.LEVEL_GENERATION_MAX_HEIGHT,
		SIM.SIMULATION_MAX_SIZE.y + SIM.GRID_SIZE * SIM.LEVEL_GENERATION_MAX_HEIGHT
	);
	const { x: viewPortX, y: viewPortY } = getViewportSize();
	_cameraPosition.x = MISC.clamp(_cameraPosition.x, ISOMETRIC_CANVAS_RECT.left, ISOMETRIC_CANVAS_RECT.right - viewPortX);
	_cameraPosition.y = MISC.clamp(_cameraPosition.y, ISOMETRIC_CANVAS_RECT.top, ISOMETRIC_CANVAS_RECT.bottom - viewPortY);
};

// Ensures that all pending operations for this frame are completed.
export const endFrame = () =>
{
	RenderQueue.executeRenderQueue();
	const { x, y } = _cameraPosition;
	for(const layer of layersMatching({ sparse: true }))
	{
		const parallax = layer.id === BACKGROUND_LAYER ? BACKGROUND_PARALLAX_FACTOR : 1;
		const renderX = -(layer.offset?.x || 0) - x * parallax;
		const renderY = -(layer.offset?.y || 0) - y * parallax;
		const canvas = layer.canvas;
		canvas.style.left = `${renderX}px`;
		canvas.style.top = `${renderY}px`;
	}
	const perfEnd = performance.now();
	const perfDiff = perfEnd - frameStartTime;
};

// Draws text on the screen at the specified x/y coordinates and optionally a style.
export const text = (layer, text, x, y, style) =>
{
	const ctx = CONTEXT(layer);
	ctx.fillStyle = style || "black";
	ctx.fillText(text, x, y);
};

/*
 * Renders a sprite by name using a configuration object. Sensible defaults will be assumed
 * for any missing properties.
 * layer: Which render layer to operate on. (Defaults to ENTITY_LAYER.)
 * spriteName: The name of the sprite to draw. (Defaults to "missing", which indicates a bug.)
 * x, y: Where to draw the sprite's origin. (Origin point is specified by the sprite definition.)
 * width, height: The dimensions of the sprite. (Each defaults to the sprite's natural dimensions.)
 * renderPriority: Number for z-ordering, where higher priorities are displayed on top. (Defaults to ENTITY_LAYER_DEFAULT_PRIORITY.)
 * spriteFrame: Index of which frame of the sprite to render. (Defaults to 0.)
 * Since draw calls must be put in to a render queue for sorting, most of the logic
 * is based around composing an executor that correctly performs the actual draw call.
 * Returns a promise that resolves after the sprite is drawn.
 */
export const renderSprite = (config) =>
{
	const
		{
			layer = ENTITY_LAYER,
			spriteName = "missing",
			x = 0, y = 0, width, height,
			renderPriority = ENTITY_LAYER_DEFAULT_PRIORITY,
			spriteFrame = 0
		} = config;
	if(spriteName === "none" && !window.debugVars.forceRenderNoneSprites){return;}
	const spriteInfo = SPRITE_DEFINITIONS[spriteName || "missing"] || SPRITE_DEFINITIONS.missing;
	const layerProps = getLayerProps(layer);
	const finalizeParamsWithAssetData = spriteAsset =>
	{
		const retVal = Object.assign({
			spriteSheetX: 0,
			spriteSheetY: 0,
			spriteSheetWidth: spriteAsset.width,
			spriteSheetHeight: spriteAsset.height,
			originOffsetX: 0,
			originOffsetY: 0
		}, spriteInfo);
		const framesX = retVal.spriteFrames?.x || 1;
		const frameStepX = retVal.spriteFrameOffset?.x || retVal.spriteSheetWidth;
		const frameStepY = retVal.spriteFrameOffset?.y || retVal.spriteSheetHeight;
		retVal.spriteSheetX += (spriteFrame % framesX) * frameStepX;
		retVal.spriteSheetY += Math.floor(spriteFrame / framesX) * frameStepY;
		return retVal;
	};
	const renderSpriteInternal = (spriteAsset, renderPos, renderParams) =>
	{
		const scaleX = width !== undefined ? width / renderParams.spriteSheetWidth : 1;
		const scaleY = height !== undefined ? height / renderParams.spriteSheetHeight : 1;
		const offsetX = renderParams.originOffsetX * -scaleX;
		const offsetY = renderParams.originOffsetY * -scaleY;
		const destinationCoords =
		[
			Math.round(renderPos.x + offsetX), Math.round(renderPos.y + offsetY),
			width || renderParams.spriteSheetWidth, height || renderParams.spriteSheetHeight
		];
		if(window.debugVars.drawFlatMats)
		{
			layerProps.context.fillStyle =
			[
				"#000000",
				"#0000ff",
				"#00ff00",
				"#00ffff",
				"#ff0000",
				"#ff00ff",
				"#ffff00",
				"#ffffff"
			][spriteRenderCount % 8];
			layerProps.context.fillRect(...destinationCoords);
		}
		else
		{
			layerProps.context.drawImage(
				spriteAsset,
				renderParams.spriteSheetX, renderParams.spriteSheetY, renderParams.spriteSheetWidth, renderParams.spriteSheetHeight,
				...destinationCoords
			);
		}
		spriteRenderCount++;
	};
	const spriteLoadPromise = loadSprite(spriteInfo);
	let renderPos = { x, y };
	let renderQueueExecutorPreApplyCallback;
	switch(spriteInfo.projection)
	{
	case "isometric":
		renderPos = transformLayerVectorToScreenVector(layerProps, renderPos);
		renderQueueExecutorPreApplyCallback = () =>
		{
			switchRenderTransform(layer, "identity", layerProps.offset);
		};
		break;
	case "cartesian":
	case undefined:
		renderQueueExecutorPreApplyCallback = () =>
		{
			switchRenderTransform(layer, layerProps.defaultTransform, layerProps.offset);
		};
		break;
	default: LOG.error("Not configured to render sprite projections of type", spriteInfo.projection);
	}
	return new Promise(resolve =>
	{
		const executor = () => spriteLoadPromise.then(spriteAsset =>
		{
			if(renderQueueExecutorPreApplyCallback){renderQueueExecutorPreApplyCallback();}
			renderSpriteInternal(spriteAsset, renderPos, finalizeParamsWithAssetData(spriteAsset));
			resolve();
		});
		if(renderPriority === RENDER_PRIORITY_IMMEDIATE){executor();}
		else{RenderQueue.add([renderPriority, renderPos.y], executor);}
	});
};

// Returns the number of sprites that have been rendered since the last time this function was called.
let spriteRenderCount = 0;
export const resetSpriteRenderCount = () =>
{
	const retVal = spriteRenderCount;
	spriteRenderCount = 0;
	return retVal;
};

/*
 * Loads a sprite asset by its spriteInfo and returns a promise that resolves when the image is loaded.
 * Once the image is loaded, this method will execute synchronously.
 */
export const loadSprite = (spriteInfo) =>
{
	const asset = SPRITE_ASSETS[spriteInfo.file];
	if(!asset)
	{
		LOG.log("Creating new image asset for", spriteInfo.file);
		let retVal = SPRITE_ASSETS[spriteInfo.file] = new Image();
		retVal.src = `/assets/${spriteInfo.file}`;
		return retVal.promise = new Promise(resolve =>
		{
			retVal.onload = () =>
			{
				LOG.trace(`Loaded image asset: ${retVal.src}`);
				delete retVal.promise;
				resolve(retVal);
			};
		});
	}
	else if(asset.promise){return asset.promise;}
	return Promise.resolve(asset);
};

/*
 * Renders an entity according to its render state and the provided deltaTime.
 * Also renders some Entity decorations as appropriate.
 */
export const renderEntity = (ent, deltaTime) =>
{
	const ctx = CONTEXT(ENTITY_LAYER);
	const renderState = getRenderState(ent);
	const pos = renderState.tick(deltaTime);
	const { left, top, right, bottom } = renderState.getBoundingBox();
	if(ent.spriteName !== "none")
	{
		RenderQueue.add(
			[ENTITY_LAYER_DECORATOR_PRIORITY, bottom, left],
			() =>
			{
				switchRenderTransform(ENTITY_LAYER, "isometric");
				if(ent.selected)
				{
					ctx.strokeStyle = "#00FF00";
					ctx.strokeRect(left, top, right - left, bottom - top);
				}
				else if(!ent.isMine && ent.owner !== "ownerless")
				{
					ctx.strokeStyle = "#AA0000";
					ctx.strokeRect(left, top, right - left, bottom - top);
				}
				else if(window.debugVars.outlineAllEntities)
				{
					ctx.strokeStyle = "#AAAAAA";
					ctx.strokeRect(left, top, right - left, bottom - top);
				}
			}
		);
	}
	if(ent.isMine && ent.sightRadius)
	{
		RenderQueue.add(
			[],
			() =>
			{
				const { sightRadius: radius } = ent;
				const { x: maxX, y: maxY } = getCanvasSize(VISION_LAYER);
				const { x, y } = pos;
				if(x + radius > 0 && x - radius < maxX && y + radius > 0 && y - radius < maxY)
				{
					const visionCtx = CONTEXT(VISION_LAYER);
					visionCtx.save();
					visionCtx.beginPath();
					visionCtx.arc(pos.x, pos.y, radius, 0, 2 * Math.PI, false);
					visionCtx.clip();
					visionCtx.clearRect(pos.x - radius, pos.y - radius, radius * 2, radius * 2);
					visionCtx.restore();
				}
			}
		);
	}
	const spriteInfo = SPRITE_DEFINITIONS[ent.spriteName || "missing"];
	const projection = spriteInfo?.projection;
	let renderCoords = [left, top, right - left, bottom - top];
	if(projection === "isometric"){renderCoords = [pos.x, pos.y, right - left, bottom - top];}
	renderSprite(
		{
			layer: ENTITY_LAYER,
			spriteName: ent.spriteName,
			x: renderCoords[0],
			y: renderCoords[1],
			width: renderCoords[2],
			height: renderCoords[3]
		});
	if(renderState.deleted){ent.deleted = true;}
};

// Draws a green box with brighter green border given two { x, y } pairs
export const renderGreenbox = (greenBoxOrigin, greenBoxEnding) =>
{
	const hudCtx = CONTEXT(HUD_LAYER);
	hudCtx.beginPath();
	hudCtx.fillStyle = "#18f48622";
	hudCtx.strokeStyle = "#12c112AA";
	hudCtx.lineWidth = 2;
	hudCtx.fillRect(greenBoxOrigin.x, greenBoxOrigin.y, (greenBoxEnding.x - greenBoxOrigin.x), (greenBoxEnding.y - greenBoxOrigin.y));
	hudCtx.strokeRect(greenBoxOrigin.x, greenBoxOrigin.y, (greenBoxEnding.x - greenBoxOrigin.x), (greenBoxEnding.y - greenBoxOrigin.y));
};

// Gets an existing render state for an entity or initializes a new one as appropriate.
export const getRenderState = (ent) => (!ent.renderState?.deleted && ent.renderState) || (ent.renderState = new EntityRenderState(ent));

// Updates the TILE_ENTITY_LAYER to reflect the TileEntity at a particular GridPos.
export const updateTileEntity = (gridPos, tent) =>
{
	const renderOrigin = gridPos.toWorldPos();
	CONTEXT(TILE_LAYER).clearRect(renderOrigin.x, renderOrigin.y, SIM.GRID_SIZE, SIM.GRID_SIZE);
	const def = TileGrid.getTileEntityDefinition(tent);
	if(def.spriteName)
	{
		renderSprite(
			{
				layer: TILE_LAYER,
				spriteName: def.spriteName,
				...renderOrigin,
				width: SIM.GRID_SIZE,
				height: SIM.GRID_SIZE
			});
	}
};

// Takes level generation data and uses it to draw the static terrain layer.
export const renderLevelGenData = BG.renderLevelGenData;
