import * as GFX from "./gfx.js";
import * as SIM from "/common/simulationConstants.js";

/*
 * We need to use a grid to prevent extreme clustering that looks unnatural.
 * The grid has overlapping regions to reduce edge artifacts and visual tiling.
 */
const STARFIELD_CLUSTER_STEP = 30;
const STARFIELD_CLUSTER_SPAN = 120;
const STARFIELD_CLUSTER_STAR_COUNT = 12;
const STARFIELD_CLUSTER_STAR_COLOR_CHANCE = 0.3;
const STARFIELD_CLUSTER_STAR_SIZE = 2;

const gridPosToIndex = (x, y) => y * SIM.SIMULATION_TILE_GRID_SIZE.x + x;

// Dynamically generates a starry background.
export const renderBackground = () =>
{
	const ctx = GFX.CONTEXT(GFX.BACKGROUND_LAYER);
	const { x: maxX, y: maxY } = GFX.getCanvasSize(GFX.BACKGROUND_LAYER);
	GFX.switchRenderTransform(GFX.BACKGROUND_LAYER, "identity");
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, maxX, maxY);
	for(let x = 0; x < maxX; x += STARFIELD_CLUSTER_STEP)
	{
		for(let y = 0; y < maxY; y += STARFIELD_CLUSTER_STEP)
		{
			for(let i = 0; i < STARFIELD_CLUSTER_STAR_COUNT; i++)
			{
				if(Math.random() >= STARFIELD_CLUSTER_STAR_COLOR_CHANCE){ctx.fillStyle = "#FFFFFF";}
				else
				{
					const hexVals = [0,0,0,0,0,0].map(_ => Math.floor(Math.random() * 16).toString(16));
					ctx.fillStyle = `#${hexVals.join("")}`;
				}
				const starX = Math.floor(Math.random() * STARFIELD_CLUSTER_SPAN + x);
				const starY = Math.floor(Math.random() * STARFIELD_CLUSTER_SPAN + y);
				ctx.fillRect(starX, starY, STARFIELD_CLUSTER_STAR_SIZE, STARFIELD_CLUSTER_STAR_SIZE);
			}
		}
	}
};

/*
 * Directions are specified as bitwise values.
 * +------- DOWNRIGHT
 * |+------ DOWN
 * ||+----- DOWNLEFT
 * |||+---- LEFT
 * ||||+--- UPLEFT
 * |||||+-- UP
 * ||||||+- UPRIGHT
 * |||||||+ RIGHT
 * 01010101
 * E.G: 12 -> 00000011 -> RIGHT | UPRIGHT
 */
const RIGHT = 1;
const UPRIGHT = 2;
const UP = 4;
const UPLEFT = 8;
const LEFT = 16;
const DOWNLEFT = 32;
const DOWN = 64;
const DOWNRIGHT = 128;

/*
 * Generator function which yields data related to getting adjacent tiles by direction.
 * Takes an x, y pertaining to the square at the center.
 * Yields a series of objects with position and direction properties.
 * Position is a Vector2-like object giving the grid position of the adjacent tile.
 * Direction uses the values specified above.
 * Will skip positions that are outside the grid.
 */
function* generateAdjacentSquares(x, y)
{
	const directions =
	[
		{ x: 0, y: 0 },
		{ x: 1, y: 0 },
		{ x: 1, y: -1 },
		{ x: 0, y: -1 },
		{ x: -1, y: -1 },
		{ x: -1, y: 0 },
		{ x: -1, y: 1 },
		{ x: 0, y: 1 },
		{ x: 1, y: 1 }
	];
	for(let i in directions)
	{
		const vec = directions[i];
		const position = { x: x + vec.x, y: y + vec.y };
		if
		(
			position.x >= 0 && position.x < SIM.SIMULATION_TILE_GRID_SIZE.x &&
			position.y >= 0 && position.y < SIM.SIMULATION_TILE_GRID_SIZE.y
		)
		{
			yield { position, direction: 1 << (i - 1)};
		}
	}
}

/*
 * Provides an interface for getting sprite information about a kind of terrain at a location
 * by providing properties of that location.
 * Returned sprites are an object with spriteName and spriteFrame properties.
 */
class TileSpriteMapping
{
	// The default sprite to use when one cannot be found for the specified parameters.
	defaultSpriteInfo = { spriteName: "missing", spriteFrame: 0 };
	// Gets a terrain sprite for a tile that is fully-enclosed.
	getUndergroundSprite(){return this.defaultSpriteInfo;}
	/*
	 * Gets a terrain sprite for a surface tile.
	 * Optionally takes a bitwise number that specifies the direction(s)
	 * in which the adjacent tile is of LOWER height
	 */
	getSurfaceSprite(heightFlags){return this.defaultSpriteInfo;}
}
class SingleSpriteSheetMapping extends TileSpriteMapping
{
	// Takes the name of the sprite sheet to use.
	constructor(spriteSheetName, defaultSpriteFrame = 0)
	{
		super();
		this.defaultSpriteInfo = { spriteName: spriteSheetName, spriteFrame: defaultSpriteFrame };
	}
	/*
	 * Array of direction-aware sprite frames, indexed using the bitwise directions value.
	 * Each value is itself an array of possible values, chosen at random.
	 */
	directionalFrames = new Array(256);
	getSurfaceSprite(heightFlags)
	{
		const frames = this.directionalFrames[heightFlags];
		if(!frames){return this.defaultSpriteInfo;}
		const frame = frames[Math.floor(Math.random() * frames.length)];
		return { ...this.defaultSpriteInfo, spriteFrame: frame };
	}
}
class DarkroseMapping extends SingleSpriteSheetMapping
{
	constructor()
	{
		super("DarkroseTerrainFull", 144);
	}
	getUndergroundSprite(){return this.getSurfaceSprite(0);}
	getSurfaceSprite(heightFlags)
	{
		/*
		 * This tile set only cares about the corners, so any horizontal or
		 * vertical flags instead enable the associated corners, then get
		 * stripped themselves to match the expected bitwise values in directionalFrames.
		 */
		heightFlags |=
			((UPRIGHT | DOWNRIGHT) * !!(heightFlags & RIGHT))
			| ((UPRIGHT | UPLEFT) * !!(heightFlags & UP))
			| ((UPLEFT | DOWNLEFT) * !!(heightFlags & LEFT))
			| ((DOWNRIGHT | DOWNLEFT) * !!(heightFlags & DOWN));
		heightFlags &= UPRIGHT | UPLEFT | DOWNLEFT | DOWNRIGHT;
		return super.getSurfaceSprite(heightFlags);
	}
}
class DarkroseGrassMapping extends DarkroseMapping
{
	constructor()
	{
		super();
		const frames = this.directionalFrames;
		frames[0] = [34];
		for(let i = 1; i <= 19; i++){frames[0].push(i);}
		frames[UPLEFT | DOWNLEFT | UPRIGHT | DOWNRIGHT] = [70];
		frames[DOWNLEFT | UPRIGHT | DOWNRIGHT] = [35];
		frames[UPLEFT | UPRIGHT] = [36];
		frames[UPRIGHT | DOWNRIGHT] = [37];
		frames[UPLEFT | UPRIGHT | DOWNRIGHT] = [38];
		frames[UPRIGHT] = [40];
		frames[DOWNRIGHT] = [41];
		frames[DOWNLEFT] = [42];
		frames[UPLEFT] = [43];
		frames[DOWNLEFT | UPLEFT | DOWNRIGHT] = [44];
		frames[DOWNLEFT | UPLEFT] = [45];
		frames[DOWNLEFT | DOWNRIGHT] = [46];
		frames[DOWNLEFT | UPLEFT | UPRIGHT] = [47];
	}
}
class DarkroseRockMapping extends DarkroseMapping
{
	constructor()
	{
		super();
		const frames = this.directionalFrames;
		frames[0] = [54, 55];
		frames[UPLEFT | DOWNLEFT] = [49];
		frames[DOWNLEFT | UPLEFT | DOWNRIGHT] = [51];
		frames[DOWNLEFT | DOWNRIGHT] = [52];
		frames[UPLEFT | UPRIGHT | DOWNLEFT] = [53];
		frames[UPRIGHT | DOWNLEFT | DOWNRIGHT] = [56];
		frames[UPLEFT | UPRIGHT] = [57];
		frames[UPRIGHT | DOWNRIGHT] = [58];
		frames[UPLEFT | UPRIGHT | DOWNRIGHT] = [59];
		frames[UPLEFT | DOWNLEFT | UPRIGHT | DOWNRIGHT] =
			frames[UPLEFT]
			= frames[UPRIGHT]
			= frames[DOWNLEFT]
			= frames[DOWNRIGHT]
			= [50];
	}
}

// Takes a levelGenResult from the back-end and translates it in to draw calls to create the terrain.
export const renderLevelGenData = (levelGenResult) =>
{
	const darkroseGrass = new DarkroseGrassMapping();
	const darkroseRock = new DarkroseRockMapping();
	const maxX = SIM.SIMULATION_TILE_GRID_SIZE.x, maxY = SIM.SIMULATION_TILE_GRID_SIZE.y;
	const grid = levelGenResult.grid;
	for(let renderHeight = 0; renderHeight <= SIM.LEVEL_GENERATION_MAX_HEIGHT; renderHeight++)
	{
		for(let x = maxX - 1; x >= 0; x--)
		{
			for(let y = 0; y < maxY; y++)
			{
				const i = gridPosToIndex(x, y);
				const { height, type } = grid[i];
				if(height < renderHeight){continue;}
				const isUnderground = renderHeight !== height;
				let heightFlags = 0;
				let isVisible = !isUnderground;
				for(const result of generateAdjacentSquares(x, y))
				{
					const otherIndex = gridPosToIndex(result.position.x, result.position.y);
					const otherHeight = grid[otherIndex].height;
					if(otherHeight < renderHeight){heightFlags |= result.direction;}
					if(otherHeight <= renderHeight){isVisible = true;}
				}
				if(!isVisible){continue;}
				const tileMapping = (_ =>
				{
					if(isUnderground){return darkroseRock;}
					switch(type)
					{
					case "normal":
					default:
						switch(renderHeight)
						{
						case 0: return darkroseRock;
						case 1: return darkroseGrass;
						default: return darkroseGrass;
						}
					case "blocked": return darkroseRock;
					}
				})();
				const renderX = (x + renderHeight) * SIM.GRID_SIZE;
				const renderY = (y - renderHeight) * SIM.GRID_SIZE;
				const spriteRenderProps =
				{
					layer: GFX.TERRAIN_LAYER,
					x: renderX, y: renderY,
					renderPriority: GFX.RENDER_PRIORITY_IMMEDIATE
				};
				if(isUnderground){Object.assign(spriteRenderProps, tileMapping.getUndergroundSprite());}
				else{Object.assign(spriteRenderProps, tileMapping.getSurfaceSprite(heightFlags));}
				GFX.renderSprite(spriteRenderProps);
			}
		}
	}
};
