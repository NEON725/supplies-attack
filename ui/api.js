import * as LOG from "/common/logging.js";
import EventDTO from "/common/EventDTO.js";
import EntityRoster from "/common/EntityRoster.js";
import * as SIM from "/common/simulationConstants.js";
import * as GFX from "./gfx.js";
import * as TileGrid from "/common/TileGrid.js";

// Issues a GET request to the specified URL. Parses the response as a JSON object and resolves to that object.
export const get = (url) =>
{
	LOG.trace(`GET: ${url}`);
	return fetch(url)
		.then(response => response.json())
		.catch(reason =>
		{
			LOG.error(`Error from ${url}: ${reason}`);
			throw reason;
		});
};

let mainSocket;
let session;
// Returns a promise that resolves when the web API is initialized.
export const init = (tileGridAPIPromise, gfxAPIPromise) => Promise.all([tileGridAPIPromise, gfxAPIPromise]).then(() => new Promise((resolve, reject) =>
{
	let hasConnected = false;
	mainSocket = new WebSocket(location.origin.replace(/^http/, "ws"));
	mainSocket.onopen = () =>
	{
		LOG.trace("Main API websocket opened.");
		hasConnected = true;
		resolve();
	};
	mainSocket.onclose = (event) =>
	{
		LOG.error(`Main API websocket closed. Code ${event.code}`);
		// TODO: Page must be refreshed; Warn user.
	};
	mainSocket.onerror = (event) =>
	{
		LOG.error(`Main socket error: ${event.message}`);
		if(!hasConnected){reject(event.message);}
		// TODO: Very likely page must be refreshed; Warn user.
	};
	mainSocket.onmessage = (socketEvent) =>
	{
		const event = JSON.parse(socketEvent.data);
		// Coerce type to EventDTO since that is the only kind of object that will ever be sent by the server.
		event.__proto__ = EventDTO.prototype;
		switch(event.type)
		{
		case "FullBrief":
			// FullBrief is a snapshot of game state, which gets merged with the existing game state to apply updates.
			TileGrid.importGrid(event.tileGridData, GFX.updateTileEntity);
			for(const ent of event.entities)
			{
				EntityRoster.addEntity(ent,
					prior =>
					{
						/*
						 * If this update would promote an empty slot, make sure there is no lingering clientside state
						 * from a previous incarnation.
						 * Any state that exists serverside will be overwritten in Entity.init.
						 */
						if(prior.deleted)
						{
							prior.selected = false;
							prior.renderState = null;
						}
					},
					added =>
					{
						// Perform additional bookkeeping pertaining to state that is only tracked clientside.
						const renderState = GFX.getRenderState(added);
						added.isMine = added.owner === session.clientID;
						if(added.animationTrigger)
						{
							renderState.init(added.animationTrigger);
							added.animationTrigger = null;
						}
						else if(!renderState.isAnimationPlaying(SIM.GAME_TICK_INTERVAL_MILLIS / 2))
						{
							renderState.init();
						}
						/*
						 * Deletions are deferred to the graphics subsystem instead of taking effect immediately,
						 * as this allows animations to complete before disappearing.
						 */
						if(added.deleted)
						{
							renderState.markForDeletion();
							added.deleted = false;
						}
						if(added.selected){LOG.info("UNIT BRIEF:", added);}
					});
			}
			EntityRoster.processPendingUpdates();
			/*
			 * The client does nothing with the list of recent deletions.
			 * We call this method to prevent accumulation of excess deletion events, since
			 * EntityRoster is a common class between server and client.
			 */
			EntityRoster.swapRecentDeletions();
			break;
		case "AcknowledgeSession":
			session = event;
			LOG.trace("Acquired session ID:", session.clientID);
			break;
		case "LevelGenResult":
			GFX.renderLevelGenData(event);
			break;
		default:
			LOG.error(`Unknown event type: ${event.type}`);
			break;
		}
	};
}));

// Sends an EventDTO object to the server.
export const send = (event) =>
{
	mainSocket.send(JSON.stringify(event));
};