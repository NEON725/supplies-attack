const renderQueue = [];

/*
 * Adds a unit of work to the render queue.
 * First parameter is an array of numerics. These will be used for the sort compare function,
 * where lower values are chosen first. Subsequent entries in the array are chosen only
 * in the event of ties, so there can be multiple tiers of sorting values.
 * The executor is a callback which will be executed when the render queue is flushed.
 * The executor can return a promise, which will cause the render queue to await until it resolves.
 */
export const add = (sortColumns, executor) =>
{
	const item = { sortColumns, executor };
	for(let i in renderQueue)
	{
		const compareResult = compare(renderQueue[i].sortColumns, sortColumns);
		if(compareResult >= 0)
		{
			renderQueue.splice(i, 0, item);
			return;
		}
	}
	renderQueue.push(item);
};

const compare = (sortColumnsA, sortColumnsB) =>
{
	for(let i = 0; i < Math.min(sortColumnsA.length, sortColumnsB.length); i++)
	{
		const a = sortColumnsA[i], b = sortColumnsB[i];
		if(a < b){return -1;}
		if(a > b){return 1;}
	}
	if(sortColumnsA.length < sortColumnsB.length){return -1;}
	if(sortColumnsA.length > sortColumnsB.length){return 1;}
	return 0;
};

// Executes all pending executors in order. Will await until all pending operations are finished.
export const executeRenderQueue = async () =>
{
	for(let item of renderQueue)
	{
		const { executor } = item;
		const result = executor();
		if(result){await result;}
	}
	renderQueue.splice(0, renderQueue.length);
};
