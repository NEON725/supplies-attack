import * as GFX from "./gfx.js";
import * as LOG from "/common/logging.js";
import * as API from "./api.js";
import * as TileGrid from "/common/TileGrid.js";
import EntityRoster from "/common/EntityRoster.js";
import * as Events from "/common/EventTypes.js";

// Pixels per second.
const EDGE_PAN_SPEED = 250;

/*
 * If the mouse leaves the viewport, due to tick rate the last known vector
 * may not be on the edge. This is a tolerance value to process close-enough values.
 * NOTE: Only applies for when the mouse leaves the viewport. For edges where this is not
 * possible, exact values are checked.
 */
const EDGE_PAN_THRESHHOLD = 120;

let fps = 0;
let _frames = 0;
let leftMouseDown = false; //caches mouse state, events.buttons should be used to assign this value in event handlers
let greenBoxOrigin = {x: 0, y: 0};
let greenBoxEnding = {x: 0, y: 0};
let mousePos = {x: 0, y: 0};
let edgePanVector = {x: 0, y: 0};
let isEdgePanning = false;

setInterval(() =>
{
	fps = _frames;
	_frames = 0;
	LOG.trace(`FPS: ${fps}`);
}, 1000);

window.debugVars =
{
	forceRenderNoneSprites: false,
	outlineAllEntities: false,
	drawFlatMats: false
};

let lastMouseClick = null;

const mouseDownEvent = (event) => 
{
	// Translates the mouse position to game-window-relative position and records it.
	const bounds = GFX.CANVAS(GFX.ENTITY_LAYER).getBoundingClientRect();
	const x = event.clientX - bounds.left;
	const y = event.clientY - bounds.top;
	const button = event.buttons & 2 ? "right" : "left";
	lastMouseClick = { x, y, button };
	greenBoxOrigin = { x, y };
	greenBoxEnding = { x, y };
	if (button == "left")
	{
		leftMouseDown = true;
	}
	event.preventDefault();
	return false;
};

const mouseUpEvent = (event) =>
{
	leftMouseDown = false;
};

const mouseMoveEvent = (event) =>
{
	const bounds = GFX.CANVAS(GFX.HUD_LAYER).getBoundingClientRect();
	mousePos = { x: event.clientX - bounds.left, y: event.clientY - bounds.top };
	// Find edges of greenbox
	if(leftMouseDown)
	{
		greenBoxEnding = mousePos;
	}
	const { x: viewX, y: viewY } = GFX.getViewportSize();
	if(mousePos.x < EDGE_PAN_THRESHHOLD){edgePanVector.x = -1;}
	else if(mousePos.x > viewX - EDGE_PAN_THRESHHOLD){edgePanVector.x = 1;}
	else{edgePanVector.x = 0;}
	if(mousePos.y < EDGE_PAN_THRESHHOLD){edgePanVector.y = -1;}
	else if(mousePos.y > viewY - EDGE_PAN_THRESHHOLD){edgePanVector.y = 1;}
	else{edgePanVector.y = 0;}
	isEdgePanning = mousePos.x === 0 || mousePos.x === viewX - 1 || mousePos.y === 0 || mousePos.y === viewY - 1;
};

const mouseEnterEvent = (event) =>
{
	leftMouseDown = event.buttons ? true : false;
};

const mouseLeaveEvent = event =>
{
	isEdgePanning = true;
};

const initClient = () =>
{
	GFX.registerEventListener("mousedown", mouseDownEvent);
	GFX.registerEventListener("mouseup", mouseUpEvent);
	GFX.registerEventListener("mousemove", mouseMoveEvent);
	GFX.registerEventListener("mouseenter", mouseEnterEvent);
	GFX.registerEventListener("mouseleave", mouseLeaveEvent);

	renderLoop();
};

let lastRenderTime;
const renderLoop = async (timestamp) =>
{
	const deltaTime = lastRenderTime ? (timestamp - lastRenderTime) : 0;
	lastRenderTime = timestamp;

	if(isEdgePanning)
	{
		const cam = GFX.cameraPosition();
		cam.x += edgePanVector.x * EDGE_PAN_SPEED * deltaTime / 1000;
		cam.y += edgePanVector.y * EDGE_PAN_SPEED * deltaTime / 1000;
	}

	GFX.beginFrame();

	// If mouse is held down, create a selection box
	if(leftMouseDown)
	{
		GFX.renderGreenbox(greenBoxOrigin, greenBoxEnding);
	}

	let selection = []; // List of IDs of selected units.
	window.selectedUnits = []; // For console debugging.

	for(const ent of EntityRoster)
	{
		// Checks whether there was a click event and whether a unit should be selected or deselected.
		const bounds = GFX.getRenderState(ent).getScreenSpaceBoundingBox();
		if(leftMouseDown && ent.interactions.includes("command"))
		{
			const xl = greenBoxOrigin.x < greenBoxEnding.x ? greenBoxOrigin.x : greenBoxEnding.x;
			const yl = greenBoxOrigin.y < greenBoxEnding.y ? greenBoxOrigin.y : greenBoxEnding.y;
			const xr = greenBoxOrigin.x > greenBoxEnding.x ? greenBoxOrigin.x : greenBoxEnding.x;
			const yr = greenBoxOrigin.y > greenBoxEnding.y ? greenBoxOrigin.y : greenBoxEnding.y;
			ent.selected = xr >= bounds.left && yr >= bounds.top && xl <= bounds.right && yl <= bounds.bottom;
		}
		if(ent.selected)
		{
			selection.push(ent.id);
			window.selectedUnits.push(ent);
		}
		// Performs the main logic for rendering an entity on screen.
		GFX.renderEntity(ent, deltaTime);
	}
	
	// Issues a command to selected units.
	if(lastMouseClick?.button === "right")
	{
		const orderPosition = GFX.transformScreenVectorToWorldVector(lastMouseClick);
		API.send(new Events.SendCommandEvent(selection, orderPosition));
	}

	lastMouseClick = null;

	await GFX.endFrame();

	let sprites = GFX.resetSpriteRenderCount();
	GFX.text(GFX.HUD_LAYER, `FPS:${fps} Sprites:${sprites} Entities:${EntityRoster.getEntCountEstimate()}`, 5, GFX.getCanvasSize(GFX.HUD_LAYER).y - 5);
	_frames++;
	window.requestAnimationFrame(renderLoop);
};

const tileDefPromise = API.get("/common/TileEntityDefinitions.json")
	.catch(reason =>
	{
		LOG.error("Failed to acquire tile entity definitions:", reason);
	});

// Main entry point for clientside code.
const tileGridAPIPromise = TileGrid.init(tileDefPromise);
const gfxAPIPromise = GFX.init();
const websocketAPIPromise = API.init(tileGridAPIPromise, gfxAPIPromise);
Promise.all([tileDefPromise, tileGridAPIPromise, gfxAPIPromise, websocketAPIPromise])
	.then(initClient)
	.catch((reason) =>
	{
		document.querySelector("h1").innerText = "Cannot initialize game. Console may have more details.";
		throw reason;
	});