import { trace } from "./common/logging.js";
import * as httpAPI from "./httpAPI.js";
import * as wsAPI from "./websocketAPI.js";
import * as simAPI from "./game/SimulationMain.js";
import * as TileGrid from "supplies-attack/common/TileGrid.js";
import * as LevelGen from "supplies-attack/game/LevelGeneration.js";
import TileEntityDefinitions from "supplies-attack/common/TileEntityDefinitions.json" assert { type: "json" };

import * as UUID from "uuid";
import Entity from "supplies-attack/common/Entity.js";

// Monkey-patch Entity UUID generation since Entity.js is a shared file between client and server.
Entity.prototype.generateUUID = UUID.v4;

// Initialize all subsystems via their promises.
const tileGridAPIPromise = TileGrid.init(Promise.resolve(TileEntityDefinitions));
const levelGenPromise = LevelGen.init(tileGridAPIPromise);
const simAPIPromise = simAPI.init(tileGridAPIPromise, levelGenPromise);
const httpAPIPromise = httpAPI.init();
const wsAPIPromise = wsAPI.init(httpAPIPromise, simAPIPromise);
Promise.all([simAPIPromise, wsAPIPromise, httpAPIPromise, tileGridAPIPromise, levelGenPromise]).then(() =>
{
	trace("Server finished startup.");
});