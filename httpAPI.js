import http from "http";
import node_static from "node-static";
import {trace} from "./common/logging.js";
import {dirname, errorResponse, objectResponse} from "./serverUtils.js";

/*
 * Serving folders of static assets to the client is done via node-static dependency to avoid
 * the hassle of creating a robust sandbox.
 */
const fileServeConfig =
{
	cache: 1,
	indexFile: "index.html"
};
var commonFileServe = new node_static.Server(`${dirname()}/common/`, fileServeConfig);
var uiFileServe = new node_static.Server(`${dirname()}/ui/`, fileServeConfig);

function serveFile(server, request, response)
{
	trace(`Serving file: ${server.root} ${request.url}`);
	server.serve(request, response)
		.addListener("error", (err) =>
		{
			if (err.status === 404)
			{
				errorResponse(response, "File not found", err.status);
			}
			else
			{
				errorResponse(response, `Unknown error: ${err.message}`, err.status);
			}
		});
}

let httpServer;
export const getHttpServer = () => httpServer;

// Returns a promise that resolves when the HTTP API is initialized.
export const init = () => new Promise((resolve) =>
{
	if(httpServer)
	{
		httpServer.close();
	}
	httpServer = http.createServer((request, response) =>
	{
		/*
		 * Callback for HTTP requests are given a request and response, where the response
		 * object is just a collection of methods to provide response data.
		 */
		const reqURL = request.url;
		const pathSegments = [...reqURL.match(/\/[^/]*/g)];
		trace(`Incoming ${request.method} request: ${reqURL} ${pathSegments}`);
		// Each nested switch/case can call pathSegments.shift() to get the next segment of the URL.
		let nextSegment = pathSegments.shift();
		switch(nextSegment)
		{
		case "/common":
			// Re-assemble the rest of the URL so commonFileServe can digest the URLs.
			request.url = pathSegments.join("/");
			trace(`Serving file: ${request.url}`);
			serveFile(commonFileServe, request, response);
			break;
		case "/api":
			// Subsection for all API calls that do not require a game session and its websocket.
			nextSegment = pathSegments.shift();
			trace(`Received API request under: ${nextSegment}`);
			switch(nextSegment)
			{
			case "/heartbeat":
				objectResponse(response, {Heart: "beating"});
				break;
			default:
				errorResponse(response, `Unknown API request: ${nextSegment}`, 404);
				break;
			}
			break;
		default:
			// No processing needed since the URLs for UI files are mapped the same as the root folder for the uiFileServe.
			serveFile(uiFileServe, request, response);
			break;
		}
	});
	httpServer.listen(80);
	trace("HTTP API initialized.");
	resolve();
});