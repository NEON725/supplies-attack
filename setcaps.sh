#!/bin/bash

export CAPNAME="cap_net_bind_service"
export NODE="$(which node)"
if [[ -z "$NODE" ]]
then
	echo 'Could not find node binary. Did you remember to run "nvm use ."?'
	exit 1
fi
echo "Found node binary at $NODE"
export CAPS="$(getcap "$NODE" | grep -oFe"$CAPNAME")"
if [[ -z "$CAPS" ]]
then
	echo "Did not detect low-port capability. (Found $CAPS) Elevating to correct."
	if sudo setcap "${CAPNAME}=+ep" "$NODE"
	then
		echo "Done."
	else
		retval=$?
		echo "Could not grant low-port capability to node. App will not run."
		exit $retval
	fi
else
	echo "Binary already has correct capacilities."
fi
echo 'Remember to run "nvm use ." when launching a new terminal session.'
