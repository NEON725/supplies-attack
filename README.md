# ALL RIGHTS RESERVED

# Requirements
* NVM

# Installation
* Clone the repository.
* `nvm use .`
* `npm install`
* `npm run setcaps`

# Running
* `npm run start`
* Navigate a web browser to `http://localhost:80/`

# Development
* Always run `nvm use .` after starting a new terminal session in the repository.
* Make sure `npm run lint` is clear before submitting a PR.
