import EntityRoster from "supplies-attack/common/EntityRoster.js";
import TestUnit from "supplies-attack/game/TestUnit.js";
import * as TileGrid from "supplies-attack/common/TileGrid.js";
import * as SIM from "supplies-attack/common/simulationConstants.js";
import TileEntityDelegate from "./TileEntityDelegate.js";
import * as Common from "./common.js";

export const registerTileEntityBehaviors = (tickLibrary) =>
{
	tickLibrary[2] = function(gridPos, def, deltaTime)
	{
		const worldPos = gridPos.toWorldPos({ center: true });
		if(!this.resources)
		{
			TileGrid.createTileEntity(gridPos, { id: 1 });
		}
		else if(this.resources && (this.cooldown -= deltaTime) <= 0)
		{
			this.cooldown = 2000;
			this.resources--;
			const spawnPos = { ...worldPos };
			spawnPos.x += SIM.GRID_SIZE;
			EntityRoster.addEntity(new TestUnit(
				{
					owner: this.owner,
				},
				spawnPos));
		}
		let foundDelegate = false;
		for(const result of Common.generateEntitiesInRange(worldPos, 0))
		{
			const [ ent ] = result;
			if(ent instanceof TestBuildingDelegate){foundDelegate = true;}
		}
		if(!foundDelegate)
		{
			EntityRoster.addEntity(new TestBuildingDelegate(gridPos, this, def.scanRange));
		}
	};
};

export class TestBuildingDelegate extends TileEntityDelegate
{
	constructor(gridPos, parent, scanRange){super(gridPos, parent, { scanRange });}
	getEntityInteractionDistance(){return Math.max(super.getEntityInteractionDistance(), this.scanRange);}
	interact(interaction)
	{
		super.interact(interaction);
		if(interaction.dist <= this.scanRange)
		{
			interaction.other.makePlayerAware(this.owner);
		}
	}
}
