import Entity from "supplies-attack/common/Entity.js";
import EntityRoster from "supplies-attack/common/EntityRoster.js";

/*
 * TileEntityDelegates can be spawned by TileEntity implementations that need to tick like an Entity.
 * In performance terms, this gives the TileEntity the same processing impact as a discrete Entity and
 * should therefore be used sparingly.
 */
export default class TileEntityDelegate extends Entity
{
	// The list of properties that, if equal for two TileEntities, means the Tile is effectively the same.
	static EQUALITY_PROPERTIES = ["id", "owner", "active"];

	// Takes the grid position and a TileEntity by reference, and any number of configurators.
	constructor(gridPos, parent, ...args)
	{
		super(
			gridPos.toWorldPos({ center: true }),
			{
				/*
				 * To ensure proper cleanup when the TileEntity is removed, we take a shallow clone of the TileEntity
				 * and check for value equality each tick.
				 * Because TileEntities are fungible by design, another TileEntity with identical properties is
				 * conceptually the same tile anyway.
				 * This is performant so long as the TileEntity remains small. (as it should for unrelated but also
				 * performance reasons.)
				 */
				tileEntityClone: {...parent},
				parent,
				owner: parent.owner,
				collisionRadius: 0,
				spriteName: "none"
			},
			...args);
	}

	// TileEntities are expected to derive and override, calling super.tick(deltaTime) first.
	tick(deltaTime)
	{
		super.tick(deltaTime);
		for(const prop of TileEntityDelegate.EQUALITY_PROPERTIES)
		{
			if(this.parent[prop] !== this.tileEntityClone[prop])
			{
				EntityRoster.deleteEntity(this);
			}
		}
	}
}