import EntityRoster from "supplies-attack/common/EntityRoster.js";
import Vector2 from "vector2-node";
import * as TileGrid from "supplies-attack/common/TileGrid.js";
import * as LOG from "supplies-attack/common/logging.js";

// TileEntity implementations:
import * as TestBuilding from "./TestBuilding.js";

/*
 * Since TileEntities are not distinguished by type, we keep a library of tick methods here instead.
 * This behavior is serverside-only so imports can be used.
 */

/*
 * Selects and executes the relevant tick method for a TileEntity.
 * Takes a TileEntity struct, the grid position, and a deltaTime argument in millis.
 */
export const tick = (tent, gridPos, deltaTime) =>
{
	const def = TileGrid.getTileEntityDefinition(tent);
	const _tick = tickLibrary[tent.id];
	if(_tick){_tick.call(tent, gridPos, def, deltaTime);}
	else{LOG.warn("Tried to tick a TileEntity with no tick method!", tent);}
};

/*
 * TileEntity ticks happen at irregular intervals that can be quite long.
 * Each one should be called with a this object referring to the TileEntity, a parameter for the
 * GridPosition, the TileEntityDefinition, and the deltaTime.
 * To get the Function.call syntax to work properly, these must be true functions without lambda
 * syntax.
 */
const tickLibrary =
{
};
TestBuilding.registerTileEntityBehaviors(tickLibrary);

/*
 * Convenience generator that DRYs the logic for iterating over entities in range of the tile.
 * Yields [Entity, offsetPosition]
 */
export function* generateEntitiesInRange(originPos, radius)
{
	for(const ent of EntityRoster)
	{
		const diff = new Vector2(ent.x, ent.y).sub(originPos);
		if(diff.lengthSq() <= radius * radius)
		{
			yield [ent, diff];
		}
	}
}