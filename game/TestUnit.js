import Unit from "./Unit.js";
import UnitDefinitions from "supplies-attack/common/UnitDefinitions.json" assert {type: "json"};
import EntityRoster from "supplies-attack/common/EntityRoster.js";
import * as SIM from "supplies-attack/common/simulationConstants.js";
import Projectile from "./Projectile.js";

export default class TestUnit extends Unit
{
	constructor(...configs)
	{
		super("TestUnit",
			{
				spriteName: "Scarab",
				attackCooldown: 0
			}, ...configs);
	}
	tick()
	{
		super.tick();
		this.attackCooldown -= SIM.GAME_TICK_INTERVAL_MILLIS;
	}
	interact(interaction)
	{
		super.interact(interaction);
		if(this.attackCooldown <= 0 && interaction.other instanceof Unit && interaction.other.owner !== this.owner)
		{
			this.attackCooldown = 1000;
			EntityRoster.addEntity(new Projectile(
				{
					target: interaction.other,
					duration: 500,
					x: this.x,
					y: this.y,
					owner: this.owner
				}
			));
		}
	}
}

UnitDefinitions.TestUnit.class = TestUnit;