import * as SIM from "supplies-attack/common/simulationConstants.js";
import Entity from "supplies-attack/common/Entity.js";
import EntityRoster from "supplies-attack/common/EntityRoster.js";
import Vector2 from "vector2-node";

/*
 * Projectile is a type of entity that travels to a destination over a
 * specified time, performs an interaction event at the destination, then is deleted.
 */
export default class Projectile extends Entity
{
	constructor(...configs)
	{
		super(
			{
				target: null, // Target can either be an Entity, Vector2, or have similar properties to either.
				duration: SIM.GAME_TICK_INTERVAL_MILLIS,
				interpolationMethod: "quadInOut",
				contactMethod: "collision",
				contactRadius: null,
				contactTesting: false
			},
			...configs);
		this.init(
			{
				contactRadius: this.contactRadius || this.collisionRadius,
				collisionRadius: 0
			});
		this.triggerAnimation(
			{
				method: this.interpolationMethod,
				duration: this.duration,
				destination: new Vector2(this.target.x, this.target.y)
			});
	}
	tick()
	{
		super.tick();
		if(this.target instanceof Entity)
		{
			this.target.makePlayerAware(this.target.owner);
		}
		if(this.target.deleted || this.contactTesting)
		{
			EntityRoster.deleteEntity(this);
		}
		else if((this.duration -= SIM.GAME_TICK_INTERVAL_MILLIS) <= 0)
		{
			this.contactTesting = true;
			this.x = this.target.x;
			this.y = this.target.y;
			switch(this.contactMethod)
			{
			case "collision":
			default:
				this.collisionRadius = this.contactRadius;
				break;
			}
		}
	}
	interact(interaction)
	{
		super.interact(interaction);
		if(this.contactTesting && !this.deleted)
		{
			switch(this.contactMethod)
			{
			case "collision":
			default:
				if(interaction.isCollision)
				{
					EntityRoster.deleteEntity(interaction.other);
					EntityRoster.deleteEntity(this);
				}
				break;
			}
		}
	}
}