import * as EventTypes from "supplies-attack/common/EventTypes.js";
import * as SIM from "supplies-attack/common/simulationConstants.js";
import SimplexNoise from "simplex-noise";

const SIMPLEX_SCALE = 1 / 16;

let levelGenData;

const gridPosToIndex = (x, y) => y * SIM.SIMULATION_TILE_GRID_SIZE.x + x;

export const init = (tileGridPromise) => tileGridPromise.then(() =>
{
	const heightMapNoise = new SimplexNoise((new Date()).toISOString());
	levelGenData = {};
	const maxX = SIM.SIMULATION_TILE_GRID_SIZE.x, maxY = SIM.SIMULATION_TILE_GRID_SIZE.y;
	const grid = levelGenData.grid = new Array(maxX * maxY);
	for(let x = 0; x < maxX; x++)
	{
		for(let y = 0; y < maxY; y++)
		{
			const i = gridPosToIndex(x, y);
			const gridDat = grid[i] = {};
			gridDat.height = Math.floor((heightMapNoise.noise2D(x * SIMPLEX_SCALE, y * SIMPLEX_SCALE) + 1) * SIM.LEVEL_GENERATION_MAX_HEIGHT / 2);
			/*
			 * Most implementations of random use a [0,1) range, so multiplying and flooring gives a range of integers from
			 * [0,n) an equal chance of appearing. Simplex gives us a range of [-1, 1], and even after correcting the range
			 * to [0,1], the inclusivity of the maximum value results in an extremely low chance of generating a height
			 * level outside of this range. Clamping the value influences the chance of the highest level slightly, but
			 * is acceptable compared to having to account for the edge case everywhere else in the code.
			 */
			if(gridDat.height === SIM.LEVEL_GENERATION_MAX_HEIGHT){gridDat.height--;}
			gridDat.type = "normal";
		}
	}
	for(let x = 0; x < maxX; x++)
	{
		for(let y = 0; y < maxY; y++)
		{
			const dat = grid[gridPosToIndex(x, y)];
			const height = dat.height;
			for(let o = 1; o <= height; o++)
			{
				const otherIndex = gridPosToIndex(x + o, y - o);
				const otherDat = grid[otherIndex];
				const minHeight = height - o;
				if(otherDat?.height < minHeight){otherDat.type = "blocked";}
			}
		}
	}
});

export const createClientBrief = () => new EventTypes.LevelGenResultEvent(levelGenData);