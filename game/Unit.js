import Entity from "supplies-attack/common/Entity.js";
import UnitDefinitions from "supplies-attack/common/UnitDefinitions.json" assert { type: "json" };
import * as SIM from "supplies-attack/common/simulationConstants.js";
import Vector2 from "vector2-node";

/*
 * A unit is a type of entity that can move about on its own by following orders.
 * Much of the statistics of units is kept in an accompanying JSON file for organization's sake
 * when making lots of balance changes.
 */
export default class Unit extends Entity
{
	constructor(unitTypeName, ...configs)
	{
		super(
			{
				unitTypeName,
				interactions: ["command"],
				command: null,
				movementSpeed: 50
			}, UnitDefinitions[unitTypeName], ...configs);
	}

	tick()
	{
		super.tick();
		if(this.command)
		{
			const diff = new Vector2(this.command.x, this.command.y).sub(this);
			const length2 = diff.lengthSq();
			const tickMoveSpeed = this.movementSpeed * SIM.GAME_TICK_INTERVAL_MILLIS / 1000;
			if (length2 <= tickMoveSpeed * tickMoveSpeed)
			{
				this.applyMotion(diff);
				this.command = null;
			}
			else
			{
				this.applyMotion(diff.scale(tickMoveSpeed / Math.sqrt(length2)));
			}
		}
	}
}