import * as LOG from "supplies-attack/common/logging.js";
import * as SIM from "supplies-attack/common/simulationConstants.js";
import * as API from "supplies-attack/websocketAPI.js";
import * as Events from "supplies-attack/common/EventTypes.js";
import * as TileGrid from "supplies-attack/common/TileGrid.js";
import * as TileEntityBehaviors from "./TileEntities/common.js";
import * as LevelGen from "supplies-attack/game/LevelGeneration.js";
import EntityRoster from "supplies-attack/common/EntityRoster.js";
import Vector2 from "vector2-node";
import { DeletedEntity } from "../common/Entity.js";
import GridPos from "../common/GridPos.js";

let gameLoopHandle;
let tileEntityUpdateLoopHandle;
// Returns a promise that resolves when the game logic is initialized.
export const init = (tileGridAPIPromise, levelGenPromise) => Promise.all([tileGridAPIPromise, levelGenPromise]).then(() => new Promise((resolve) =>
{
	shutdown();
	stagedEntUpdates = {};
	gameLoopHandle = setInterval(tick, SIM.GAME_TICK_INTERVAL_MILLIS);
	const tileEntityTickGenerator = generateTileEntityTick();
	tileEntityUpdateLoopHandle = setInterval(tileEntityTickGenerator.next.bind(tileEntityTickGenerator), 1);
	resolve();
}));

// Tears down game state and logic.
export const shutdown = () =>
{
	EntityRoster.clear();
	if(gameLoopHandle)
	{
		clearInterval(gameLoopHandle);
		gameLoopHandle = null;
	}
	if(tileEntityUpdateLoopHandle)
	{
		clearInterval(tileEntityUpdateLoopHandle);
		tileEntityUpdateLoopHandle = null;
	}
};

let stagedEntUpdates;

// Main iteration method which is called every time game mechanics are performed.
export const tick = () =>
{
	const perfStart = performance.now();

	// Each connected client gets their own, customized version of the game state according to fog of war limitations.
	const visibleGameStates = {};
	const sessions = API.getActiveSessions();
	for(const session of sessions)
	{
		visibleGameStates[session.clientID] =
		{
			entities: [],
			tileGridVisibilityMask: session.clearTileGridVisibilityMask()
		};
	}

	// We call the iterator functions directly instead of using for...of syntax because we need to access the i property later.
	const entIterator = EntityRoster[Symbol.iterator]();
	let iter;
	while(!(iter = entIterator.next()).done)
	{
		const ent = iter.value;
		// Some updates get staged for later so API responses don't hang performing independant roster iterations.
		const update = stagedEntUpdates[ent.id];
		if(update)
		{
			const owner = update.require?.owner;
			if(!owner || owner === ent.owner){ent.init(update);}
		}
		ent.tick();
		const entPos = new Vector2(ent.x, ent.y);
		const collisionRadius = ent.collisionRadius;
		const interactionDist = ent.getEntityInteractionDistance();
		/*
		 * This sub-iteration performs the n^2 logic for entities interacting with each other.
		 * We start at iter.i + 1 to reduce redundant iterations. Since each interaction is
		 * bidirectional, we don't need to later process interactions going the other direction.
		 */
		for(const otherEnt of EntityRoster.customIteration(iter.i + 1))
		{
			/*
			 * Interactions between entities are calculated in advance so they only need to be
			 * performed once for each pair of entities. Any logic that can be performed once
			 * is done here, but logic that must be done for each individual participant is done
			 * in the Entity.tick method.
			 */
			const otherCollisionRadius = otherEnt.collisionRadius;
			const otherInteractionDist = otherEnt.getEntityInteractionDistance();
			const combinedInteractionDist = Math.max(interactionDist, otherInteractionDist);
			const combinedInteractionDistSqr = combinedInteractionDist * combinedInteractionDist;
			const offset = new Vector2(entPos).sub(otherEnt);
			const distSqr = offset.lengthSq();
			if(distSqr <= combinedInteractionDistSqr)
			{
				const interaction =
				{
					offset,
					dist: Math.sqrt(distSqr),
					other: ent,
					collisionResolutionDistance: collisionRadius + otherCollisionRadius
				};
				interaction.isCollision = (collisionRadius && otherCollisionRadius) ? interaction.dist <= interaction.collisionResolutionDistance : false;
				otherEnt.interact(interaction);
				interaction.other = otherEnt;
				interaction.offset.scale(-1);
				ent.interact(interaction);
			}
		}
		// Adds the entity to the player-visible slice of game state if the player was made aware of them this tick.
		for(const session of sessions)
		{
			const cID = session.clientID;
			const gameState = visibleGameStates[cID];
			const entList = gameState.entities;
			if(cID === ent.owner)
			{
				const visibilityMask = gameState.tileGridVisibilityMask;
				TileGrid.applySightRadiusToVisibilityMask(visibilityMask, ent);
			}
			if(ent.playersAware.includes(cID))
			{
				entList.push(ent);
			}
			// The falling-edge of player awareness is treated by clients as an entity deletion to prevent fog-of-war exploits.
			else if(ent.lastTickPlayersAware.includes(cID))
			{
				entList.push(new DeletedEntity(ent));
			}
		}
		ent.lastTickPlayersAware = ent.playersAware;
		ent.playersAware = [ent.owner];
	}
	stagedEntUpdates = {};
	EntityRoster.processPendingUpdates();
	// Adds deletion events to the visible game states, since deleted entities would not be included in the previous loop.
	// Also issues the events to their client via the websocket.
	const recentDeletions = EntityRoster.swapRecentDeletions();
	for(const session of sessions)
	{
		const event = new Events.FullBriefEvent(visibleGameStates[session.clientID]);
		for(const deleted of recentDeletions)
		{
			event.entities.push(deleted);
		}
		event.tileGridData = (session.tileGridData = TileGrid.exportGrid(session.tileGridData, event.tileGridVisibilityMask));
		delete event.tileGridVisibilityMask;
		session.send(event);
	}
	const perfEnd = performance.now(), perfDuration = Math.ceil(perfEnd - perfStart);
	// Change the importance of the tick end message according to how much our processor is in pain.
	const log = (perfDuration <= 10) ? LOG.debug : ((perfDuration <= 50) ? LOG.info : ((perfDuration <= SIM.GAME_TICK_INTERVAL_MILLIS) ? LOG.log : LOG.warn));
	log("Tick End (Took", perfDuration, "ms) (Approx.", EntityRoster.getEntCountEstimate(), "Entities)");
};

export const onJoin = (session) =>
{
	// Current game prototype just hands a bunch of units to players when they join.
	const clusterX = Math.random() * 600, clusterY = Math.random() * 200;
	const gridPos = GridPos.fromWorldPos(clusterX, clusterY);
	TileGrid.createTileEntity(gridPos, { id: 2, owner: session.clientID });
	session.send(LevelGen.createClientBrief());
};

export const onClientEvent = (session, event) =>
{
	switch(event.type)
	{
	case "SendCommand":
		// Processes a request by a client to issue orders to selected units.
		for(const id of event.selection)
		{
			stagedEntUpdates[id] =
			{
				command: new Vector2(event.dest.x, event.dest.y),
				require: { owner: session.clientID }
			};
		}
		break;
	default:
		LOG.warn("Unknown client event:", event.type);
		break;
	}
};

/*
 * Generator function that runs forever, triggering tick events for TileEntities.
 * Yields after some configurable number of tick events, or if the loop is going
 * fast enough that extra tick events are not necessary.
 */
export function* generateTileEntityTick()
{
	let ticksLeftInPass = 0;
	let perfDiff = 0;
	while(true)
	{
		let tentCount = 0;
		const perfStart = performance.now();
		const minimumNextTick = perfStart + 1000;
		for(const result of TileGrid.generateActiveTileEntities())
		{
			TileEntityBehaviors.tick(result[0], result[1], perfDiff);
			tentCount++;
			if(--ticksLeftInPass <= 0)
			{
				EntityRoster.processPendingUpdates();
				yield;
				ticksLeftInPass = SIM.ACTIVE_TILE_ENTITIES_PER_PASS;
			}
		}
		EntityRoster.processPendingUpdates();
		let perfEnd = performance.now();
		perfDiff = perfEnd - perfStart;
		LOG.debug("Performed complete TileEntity pass in", Math.floor(perfDiff), "ms (", tentCount, " TileEntities)");
		while((perfEnd = performance.now()) < minimumNextTick){yield;}
		perfDiff = perfEnd - perfStart;
	}
}