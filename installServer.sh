#!/bin/bash

if [[ "$USER" == "root" ]]
then
	export SCRIPT_NAME="supplies-attack-server.service"
	export INSTALL_DIR="/etc/systemd/system"
	export SCRIPT_PATH="$REPO_DIR/$SCRIPT_NAME"
	export INSTALL_DEST="$INSTALL_DIR/$SCRIPT_NAME"
	echo "Script set the repository directory to $REPO_DIR, copy $SCRIPT_PATH to $INSTALL_DEST, and set the service account to $SERVICE_ACCOUNT."
	read -t 10 -p "Continue [Y/n]? " response
	if [[ "$?" != 0 ]]
	then
		response=n
		echo ""
	fi
	if [[ "$response" == "Y" ]] || [[ "$response" == "y" ]] || [[ "$response" == "" ]]
	then
		echo "Stopping existing service..."
		systemctl stop "$SCRIPT_NAME"
		echo "Installing..."
		cp -v "$SCRIPT_PATH" "$INSTALL_DEST"
		echo "User=$SERVICE_ACCOUNT" | tee -a "$INSTALL_DEST"
		echo "WorkingDirectory=$REPO_DIR" | tee -a "$INSTALL_DEST"
		systemctl daemon-reload
		systemctl reenable "$SCRIPT_NAME"
		systemctl start "$SCRIPT_NAME"
		echo "Installation complete."
	else
		echo "Aborting."
		exit 1
	fi
else
	export SELF="$(readlink -f "$0")"
	export REPO_DIR="$(dirname "$SELF")"
	export SERVICE_ACCOUNT="$(stat -c '%U' "$REPO_DIR")"
	cd "$REPO_DIR"
	echo "Script will elevate to install systemd service."
	sudo SERVICE_ACCOUNT="$SERVICE_ACCOUNT" REPO_DIR="$REPO_DIR" "$SELF"
	exit $?
fi
