import { WebSocketServer } from "ws";
import { trace, error } from "supplies-attack/common/logging.js";
import * as httpAPI from "./httpAPI.js";
import * as GAME from "supplies-attack/game/SimulationMain.js";
import Session from "./Session.js";
import EventDTO from "supplies-attack/common/EventDTO.js";
import * as Events from "supplies-attack/common/EventTypes.js";

let wsServer;
export const getWSS = () => wsServer;

const sessions = [];
export const getActiveSessions = () => sessions.filter(session => session.active);

// Returns a promise that resolves when the websocket API is initialized.
export const init = (httpAPIPromise, simAPIPromise) => Promise.all([httpAPIPromise, simAPIPromise]).then(() => new Promise((resolve) =>
{
	shutdown();
	/*
	 * Providing the HTTP server allows the two to share the same port.
	 * The web server will automatically respond to websocket requests and hand-off.
	 */
	wsServer = new WebSocketServer({ server: httpAPI.getHttpServer() });
	/*
	 * The callback here is confusing. First parameter is the actual websocket
	 * we'll be using for most communication hereafter. Request.socket has methods
	 * that pertain only to raw communication using the original HTTP request.
	 * I suspect these are backed by the same state since the websocket protocol
	 * re-uses the same socket as the HTTP request by design, but we still have
	 * to distinguish between them because they have different instance methods
	 * that perform actions relevant to their domain.
	 */
	wsServer.on("connection", (socket, request) =>
	{
		const clientAddress = request.socket.remoteAddress;
		trace("CL Connect:", clientAddress);
		const authClientID = authenticateIncomingConnection(socket, request);
		const session = initSession(socket, authClientID);
		socket.on("message", (data) =>
		{
			// TODO: Apply really aggressive filters to incoming events.
			const event = JSON.parse(data);
			event.__proto__ = EventDTO.prototype;
			trace("CL Receive:", session.clientID, event);
			GAME.onClientEvent(session, event);
		});
		socket.on("error", (err) =>
		{
			trace("CL Error:", session.clientID, clientAddress, err);
		});
		socket.on("close", (code, reason) =>
		{
			trace("CL Close:", session.clientID, code, reason.toString());
			session.active = false;
		});
		session.send(new Events.AcknowledgeSessionEvent(session));
		GAME.onJoin(session);
	});
	wsServer.on("error", (err) =>
	{
		error(`WSS error: ${err}`);
	});
	wsServer.on("close", () =>
	{
		trace("WSS closed.");
	});
	trace("WS API initialized.");
	resolve();
}));

// Tears down the web socket server.
export const shutdown = () =>
{
	if(wsServer){wsServer.shutdown();}
};

// Given an incoming connection, verify their authentication and assign an ID, or null for guests.
export const authenticateIncomingConnection = (socket, request) =>
{
	// TODO: Support persistent sessions via login.
	trace(`Assigning guest ID for ${request.socket.remoteAddress}`);
	return null;
};

export const generateGuestID = () => `guest-${Math.random().toString().substring(2)}`;

// Creates a new session or reactivates a stale one.
export const initSession = (socket, clientID) =>
{
	let session = clientID ? getSessionByID(clientID) : null;
	if(!session)
	{
		session = new Session(socket, generateGuestID());
		sessions.push(session);
	}
	else if(session.socket){socket.close();}
	session.socket = socket;
	session.active = true;
	return session;
};

export const getSessionByID = (clientID) => sessions.filter(session => session.clientID === clientID)[0];