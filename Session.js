import * as LOG from "supplies-attack/common/logging.js";
import { generateEmptyVisibilityMask } from "supplies-attack/common/TileGrid.js";

/*
 * Contains all information pertaining to a particular client and their
 * current session with the game server.
 */
export default class Session
{
	constructor(socket, clientID)
	{
		this.socket = socket;
		this.clientID = clientID;
		this.active = true;
		// Cached per-player because regenerating this data structure was expensive.
		this.tileGridVisibilityMask = generateEmptyVisibilityMask();
		// Also cached but this gets initialized in SimulationMain.
		this.tileGridData = null;
		LOG.trace(`New session created for ${clientID}`);
	}
	clearTileGridVisibilityMask()
	{
		this.tileGridVisibilityMask.forEach((_unused, index, arr) => arr[index] = false);
		return this.tileGridVisibilityMask;
	}
	send(obj)
	{
		this.socket.send(JSON.stringify(obj));
	}
}