import * as SIM from "./simulationConstants.js";
import * as MISC from "./misc.js";
import * as TileGrid from "./TileGrid.js";
import EntityReference from "./EntityReference.js";
import GridPos from "./GridPos.js";

/*
 * An entity is a type of game object that is non-fungible and does not conform to the grid.
 * They also have animations and movement interpolation on the client side.
 */
export default class Entity
{
	constructor(...configs)
	{
		this.init(...configs);
	}

	// This function gets monkey-patched on the back-end since node_modules aren't available on the front-end.
	generateUUID() {return "noid";}

	/*
	 * Separate init function to support object reuse.
	 * Takes an arbitrary number of arguments, each one being an object whose properties
	 * are applied to the Entity. (configurator pattern)
	 * Derived classes should mimic this pattern when overriding and may add any number of their
	 * own configurator objects.
	 */
	init(...configs)
	{
		Object.assign(this,
			(this.deleted !== false) ?
				{
					id: this.generateUUID(),
					spriteName: "missing",
					x: 0,
					y: 0,
					width: 32,
					height: 32,
					deleted: false,
					unitTypeName: "Object",
					interactions: [],
					animationTrigger: null,
					owner: "ownerless",
					collisionRadius: undefined,
					sightRadius: 0,
					lastTickPlayersAware: [],
					playersAware: [],
					isFirstTick: true,
					arrestMotion: {}
				} : {},
			...configs);
		if(this.collisionRadius === undefined){this.collisionRadius = (this.width + this.height) / 4;}
		return this;
	}

	// Sends a request to the client to trigger an animation.
	// EntityRenderState is not available in a back-end context so this is merely a request the client uses to initialize
	triggerAnimation(opts)
	{
		this.animationTrigger = opts || {};
	}

	// All derived classes should call super.tick() at the start of their own tick methods.
	tick()
	{
		if(this.isFirstTick){this.isFirstTick = false;}
		else{this.animationTrigger = null;}
		Object.assign(this.arrestMotion, { xNeg: false, xPos: false, yNeg: false, yPos: false });
		for(const result of TileGrid.generateTileEntitiesInRange(GridPos.fromWorldPos(this), this.collisionRadius + SIM.GRID_SIZE))
		{
			const def = TileGrid.getTileEntityDefinition(result[1]);
			this.interactWithTileEntity(...result, def);
		}
	}

	/*
	 * Adds the Vector2-like object to the current position, accounting for arrested motion we received from TileEntity collisions.
	 * Derived classes should use this method instead of modifying the position directly most of the time.
	 */
	applyMotion(vector2)
	{
		const { xNeg, xPos, yNeg, yPos } = this.arrestMotion;
		this.x += MISC.clamp(vector2.x, xNeg ? 0 : -Infinity, xPos ? 0 : Infinity);
		this.y += MISC.clamp(vector2.y, yNeg ? 0 : -Infinity, yPos ? 0 : Infinity);
	}

	// Gives the rectangular bounds around either the entity's position or a hypothetical position, using the entity's height and width.
	getBoundingBox(pos)
	{
		const _pos = pos || this;
		return { left: _pos.x - this.width / 2, top: _pos.y - this.height / 2, right: _pos.x + this.width / 2, bottom: _pos.y + this.height / 2};
	}

	/*
	 * The maximum distance at which the entity should generate interaction events.
	 * Interactions can still occur outside this radius if the other Entity has a much
	 * larger radius, so no assumptions should be made about whether an interaction
	 * is actually valid.
	 */
	getEntityInteractionDistance()
	{
		return Math.max(this.collisionRadius, this.sightRadius);
	}

	/*
	 * Handle an interaction event, and respond with either sight radius logic or collision logic as appropriate.
	 * Derived classes should call super.interact(interaction) at the start of their own implementation.
	 * If the other Entity has a much larger interaction radius, this method might be called with an invalid
	 * interaction. No assumptions should be made about how close an Entity is purely based on the fact that
	 * an interaction event was triggered.
	 */
	interact(interaction)
	{
		if(interaction.isCollision)
		{
			const radius = interaction.collisionResolutionDistance;
			const overlap = radius - interaction.dist;
			// Used to resolve collisions in the rare case where two Entities are at precisely the same point.
			if(!interaction.dist)
			{
				this.applyMotion({ x: Math.random() - 0.5, y: Math.random() - 0.5 });
			}
			else
			{
				const offsetMultiplier = SIM.COLLISION_RESOLUTION_MULTIPLIER * overlap / interaction.dist;
				this.applyMotion({ x: interaction.offset.x * offsetMultiplier, y: interaction.offset.y * offsetMultiplier });
			}
		}
		else if(interaction.dist <= this.sightRadius){interaction.other.makePlayerAware(this.owner);}
		this.x = MISC.clamp(this.x, 0, SIM.SIMULATION_MAX_SIZE.x);
		this.y = MISC.clamp(this.y, 0, SIM.SIMULATION_MAX_SIZE.y);
	}

	/*
	 * Handles interaction events with TileEntities.
	 * Derived classes should call super.interact(gridPos, tileEntity, tileEntityDefinition) at the start of their own implementation.
	 * This function is executed MANY times per tick per entity. If a derived implementation is provided, be extremely cautious with
	 * runtime complexity.
	 */
	interactWithTileEntity(gridPos, tileEntity, tileEntityDefinition)
	{
		if(!tileEntityDefinition.solid){return;}

		/*
		 * TileEntity collision uses this.arrestedMotion to prevent all movement in the specified directions.
		 * This approach was used instead of applying a reactionary force because such a force could be theoretically overcome with
		 * entity cramming. The arrested motion flags instead simulate a collision with an object of infinite inertia.
		 * Note that this approach does not address the distance by which the entity intersects the tile during the first
		 * tick that it collides, and thus Entities may appear to be slightly sunken in to the tile.
		 * This is not addressed because of unacceptable performance cost of calculating these collisions during every movement.
		 */
		const collisionRadius = this.collisionRadius;
		const worldPosMin = gridPos.toWorldPos();
		const gridPosMax = new GridPos(gridPos);
		gridPosMax.x += 1;
		gridPosMax.y += 1;
		const worldPosMax = gridPosMax.toWorldPos();

		/*
		 * This code generates a test point within the TileEntity's bounds that is closest to the circular entity.
		 * It also determines the type of collision so we know which axis and which direction to arrest motion
		 * when a collision is detected.
		 */
		const testPoint = {};
		const collisionTypes = {};

		// Our origin on a particular axis is either to the side of the region, or between the minimum and maximum.
		if(this.x < worldPosMin.x)
		{
			testPoint.x = worldPosMin.x;
			collisionTypes.horizontal = "xPos";
		}
		else if(this.x > worldPosMax.x)
		{
			testPoint.x = worldPosMax.x;
			collisionTypes.horizontal = "xNeg";
		}
		else
		{
			testPoint.x = this.x;
			collisionTypes.horizontal = "embed";
		}
		if(this.y < worldPosMin.y)
		{
			testPoint.y = worldPosMin.y;
			collisionTypes.vertical = "yPos";
		}
		else if(this.y > worldPosMax.y)
		{
			testPoint.y = worldPosMax.y;
			collisionTypes.vertical = "yNeg";
		}
		else
		{
			testPoint.y = this.y;
			collisionTypes.vertical = "embed";
		}

		// Check for collision as normal, then apply arrested motion selectively depending on the type of collision.
		testPoint.x -= this.x;
		testPoint.y -= this.y;
		if((testPoint.x * testPoint.x + testPoint.y * testPoint.y) < collisionRadius * collisionRadius)
		{
			const xEmbed = collisionTypes.horizontal === "embed";
			const yEmbed = collisionTypes.vertical === "embed";
			if(xEmbed && yEmbed)
			{
				this.arrestMotion.xNeg = this.arrestMotion.xPos = this.arrestMotion.yNeg = this.arrestMotion.yPos = true;
				const diffFromCenter =
				{
					x: this.x - (worldPosMax.x - worldPosMin.x) / 2,
					y: this.y - (worldPosMax.y - worldPosMin.y) / 2
				};
				const diffLength = Math.sqrt(diffFromCenter.x * diffFromCenter.x + diffFromCenter.y * diffFromCenter.y);
				this.x += diffFromCenter.x / diffLength;
				this.y += diffFromCenter.y / diffLength;
			}
			else if(xEmbed){this.arrestMotion[collisionTypes.vertical] = true;}
			else if(yEmbed){this.arrestMotion[collisionTypes.horizontal] = true;}
			else
			{
				this.arrestMotion[collisionTypes.horizontal] = true;
				this.arrestMotion[collisionTypes.vertical] = true;
			}
		}
	}

	// Adds player by ID to the list of players that can see this entity. Used for fog-of-war calculations.
	makePlayerAware(clientID)
	{
		if(!this.playersAware.includes(clientID)){this.playersAware.push(clientID);}
	}

	//Custom logic here to avoid circular references by means of using a specialized serializable entity reference.
	toJSON(key)
	{
		const retVal = this.deleted ? new DeletedEntity(this) : (key ? new EntityReference(this) : { ...this });
		return retVal;
	}
}

/*
 * Special form of Entity that provides the bare minimum of data to inform consumers that the entity is marked as deleted.
 * Missing a lot of the other properties, but in theory a deleted entity should treat those other properties as junk anyway.
 * Also good for serialization since only the ID and deletion flag are send over the wire.
 */
export class DeletedEntity
{
	constructor(ent)
	{
		this.id = ent.id;
		this.deleted = true;
	}
	// Needed because otherwise some logic elsewhere will choke if an entity-like object has no toJSON method.
	toJSON()
	{
		return this;
	}
}