import * as TileGrid from "./TileGrid.js";
import * as LOG from "./logging.js";

/*
 * TileEntities are fungible and interchangeable blocks that exist on a 2D grid.
 * Since two tiles of the same kind are indistinguishable from one another, most
 * of the runtime data is acquired from a JSON file that describes the properties
 * of the tile by numeric ID.
 * Some metadata can be encoded alongside the ID, but this should be reduced where possible.
 */
export default class TileEntity
{
	/*
	 * To avoid reinitializing a new object every time.
	 * Care should be taken not to modify this instance, including via init.
	 */
	static unspecified()
	{
		let singleton;
		return singleton || (singleton = new TileEntity());
	}

	// Uses the configurator pattern to assign values.
	constructor(...args)
	{
		this.init(...args);
	}
	init(...args)
	{
		Object.assign(this,
			{
				id: 0,
				owner: "ownerless",
				// An active TileEntity receives ticks.
				active: false,
				// Remaining properties are reserved for use by individual TileEntity implementations.
				cooldown: 0,
				resources: 0
			},
			...args);
		const def = TileGrid.getTileEntityDefinition(this);
		/*
		 * tileData is the data that should be baked directly in to the TileEntity instead of isolated
		 * to the definition. This should either be missing entirely, or be a strict subset of the
		 * values defaulted above.
		 */
		if(!def){LOG.warn("Spawned a TileEntity with no definition!", this);}
		else{Object.assign(this, def.tileData);}
	}
}