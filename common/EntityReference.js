/*
 * Object that refers to another entity by ID.
 * Intended to break circular references when converting to JSON.
 */
export default class EntityReference
{
	constructor(ent)
	{
		this.isEntityReference = true;
		this.id = ent.id;
	}
}