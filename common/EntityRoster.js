import * as LOG from "./logging.js";
import * as SIM from "./simulationConstants.js";
import Entity, { DeletedEntity } from "./Entity.js";

const noop = () => {};

class EntityRoster
{
	constructor()
	{
		this.roster = [];
		/*
		 * Some types of entity updates, like additions and deletions, are cached
		 * to reduce some classes of thrashing and inconsistent state. They
		 * are instead applied in batch when requested by the simulation logic.
		 */
		this.pendingUpdates = [];
		/*
		 * Pre-allocation reduces the strain on the memory allocator,
		 * and allowing empty slots to be marked as deleted instead of
		 * actually removed reduces garbage collection spikes.
		 */
		for(let i = 0; i < SIM.ENTITY_ROSTER_PREALLOCATION; i++)
		{
			this.roster.push(new Entity({deleted: true}));
		}
		this.entCountEstimated = 0;
		this.recentDeletions = [];
	}

	/*
	 * Adds an entity configurator to the list of pending updates.
	 * Also accepts either a postApplyCallback or a preApplyCallback and postApplyCallback.
	 * The callback methods are called before or after applying the configurators as
	 * appropriate, with the entity itself as the parameter.
	 * This allows other logic to be performed on the entity without performing redundant
	 * Roster lookups.
	 * Note that preApplyCallback is only called if there is a pre-existing entity to be updated,
	 * and it may be called with a deleted entity as the parameter.
	 */
	addEntity(newEnt, ...callbacks)
	{
		if(callbacks.length === 1){callbacks.unshift(noop);}
		const [preApply, postApply] = callbacks;
		this.pendingUpdates.push(
			{
				preApply,
				postApply,
				data: newEnt
			});
	}

	// Requests all the pending updates to be processed.
	processPendingUpdates()
	{
		for(const ent of this)
		{
			for(const update of this.pendingUpdates)
			{
				if(update.data.id === ent.id)
				{
					(update.preApply || noop)(ent);
					ent.init(update.data);
					(update.postApply || noop)(ent);
					update.done = true;
					if(update.data.deleted){LOG.trace("Deleted entity:", update.data);}
				}
			}
		}
		const rosterIterator = this[Symbol.iterator](0, true);
		let iter;
		for(const update of this.pendingUpdates)
		{
			if(!update.done && !update.data.deleted)
			{
				if(iter?.done || (iter = rosterIterator.next()).done)
				{
					const newEnt = update.data;
					const isEnt = newEnt instanceof Entity;
					const added = isEnt ? newEnt : new Entity(newEnt);
					this.roster.push(added);
					(update.postApply || noop)(added);
					LOG.info("Spawning entity:", added, "Roster size:", this.roster.length);
				}
				else
				{
					const ent = iter.value;
					const newEnt = update.data;
					const isEnt = newEnt instanceof Entity;
					(update.preApply || noop)(ent);
					ent.init(newEnt);
					ent.__proto__ = isEnt ? newEnt.__proto__ : Entity.prototype;
					(update.postApply || noop)(ent);
					LOG.info("Spawning entity:", ent);
				}
			}
		}
		this.pendingUpdates = [];
	}

	// Requests an entity to be deleted. Also records the deletion to be sent to clients in batch.
	deleteEntity(ent)
	{
		if(!ent.deleted)
		{
			const deleted = new DeletedEntity(ent);
			this.recentDeletions.push(deleted);
			this.pendingUpdates.push({ data: deleted });
			ent.deleted = true;
		}
	}

	// Hands off the list of recent deletions so the API layer can ship it to clients.
	swapRecentDeletions()
	{
		const retVal = this.recentDeletions;
		this.recentDeletions = [];
		return retVal;
	}

	/*
	 * Custom iterator for the entity roster that only considers non-deleted entities.
	 * Takes an optional parameter for the index to start from (for reducing redundant iterations)
	 * and a boolean that reverses the logic for finding empty slots.
	 * Also updates the entity count estimate whenever it completes iteration over the full roster.
	 */
	[Symbol.iterator](startFrom, findEmptySlots)
	{
		let i = (startFrom || 0) - 1;
		let found = 0;
		const retVal =
		{
			next: () => 
			{
				while(true)
				{
					if(++i >= this.roster.length)
					{
						if(!findEmptySlots){this.entCountEstimated = found;}
						return {done: true};
					}
					const retVal = this.roster[i];
					if(!!retVal.deleted === !!findEmptySlots)
					{
						found++;
						return {done: false, value: retVal, i};
					}
				}
			}
		};
		return retVal;
	}

	// Convenience function which generates an iterable using the same parameters as above.
	customIteration(...args)
	{
		return {
			[Symbol.iterator]: () => this[Symbol.iterator](...args)
		};
	}

	getEntCountEstimate() {return this.entCountEstimated;}

	clear()
	{
		for(const ent of this){ent.deleted = true;}
	}
}

const roster = new EntityRoster();
export default roster;