import EventDTO from "./EventDTO.js";

// FullBriefEvent is a complete snapshot of the game state that is known to a particular client.
export class FullBriefEvent extends EventDTO
{
	constructor(visibleGameState)
	{
		super("FullBrief", visibleGameState);
	}
	toJSON()
	{
		/*
		 * Invoke the toJSON function of entities directly so they don't get turned in to references.
		 * Any nested Entities will still get converted to references.
		 */
		return {
			...this,
			entities: this.entities.map(e => e.toJSON())
		};
	}
}

// AcknowledgeSessionEvent gives clients any session-related information they need to remember.
export class AcknowledgeSessionEvent extends EventDTO
{
	constructor(session)
	{
		super("AcknowledgeSession",
			{
				clientID: session.clientID
			});
	}
}

// SendCommandEvent is sent by clients to the server, indicating that they tried to issue an order to some number of units by ID.
export class SendCommandEvent extends EventDTO
{
	constructor(selection, dest)
	{
		super("SendCommand", { selection, dest });
	}
}

// LevelGenResultEvent is sent to clients when they initially connect and informs them of the current level layout.
export class LevelGenResultEvent extends EventDTO
{
	constructor(levelGenData)
	{
		super("LevelGenResult", levelGenData);
	}
}