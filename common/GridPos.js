import * as SIM from "./simulationConstants.js";

/*
 * GridPos is a Vector2 with convenience methods for translating to and from world position.
 * Does not include reference to Vector2 because that class is not available to clients,
 * but this is intended to be successfully type-coerced to Vector2.
 */
export default class GridPos
{
	constructor(x, y)
	{
		if(y !== undefined)
		{
			this.x = x || 0;
			this.y = y || 0;
		}
		else
		{
			Object.assign(this, x);
		}
	}
	increment(otherGridPos)
	{
		this.x += otherGridPos.x;
		this.y += otherGridPos.y;
		return this;
	}
	toWorldPos(opts)
	{
		const xOffset = opts?.center ? SIM.GRID_SIZE / 2 : 0;
		const yOffset = opts?.center ? SIM.GRID_SIZE / 2 : 0;
		return { x: this.x * SIM.GRID_SIZE + xOffset, y: this.y * SIM.GRID_SIZE + yOffset };
	}
	static fromWorldPos(x, y, opts)
	{
		const worldPos = (y !== undefined) ? { x, y } : x;
		const rounding = opts?.continuous ? n => n : Math.floor;
		return new GridPos(rounding(worldPos.x / SIM.GRID_SIZE), rounding(worldPos.y / SIM.GRID_SIZE));
	}
}