export const generateDateString = (date) => (date || new Date()).toLocaleDateString("en-us", {month:"2-digit", day:"2-digit", hour:"numeric", minute:"numeric", hour12: false});
export const generateLogPrefix = () => `${generateDateString()}:`;
// Log functions are generated because there are several of different levels of importance that should function similarly.
const generateLogFunction = (funcName) => (...params) => console[funcName](generateLogPrefix(), ...params);
const _logError = generateLogFunction("error");
export const error = (...args) =>
{
	_logError(...args);
	throw new Error(...args);
};
export const warn = generateLogFunction("warn");
export const log = generateLogFunction("log");
export const info = generateLogFunction("info");
export const debug = generateLogFunction("debug");
export const trace = debug; // DEPRECATED