import * as LOG from "./logging.js";
import * as SIM from "./simulationConstants.js";
import TileEntity from "./TileEntity.js";
import GridPos from "./GridPos.js";

export const TILE_GRID_SIZE = { x: SIM.SIMULATION_MAX_SIZE.x / SIM.GRID_SIZE, y: SIM.SIMULATION_MAX_SIZE.y / SIM.GRID_SIZE };
export const TILE_GRID_MAX_TILES = TILE_GRID_SIZE.x * TILE_GRID_SIZE.y;

// The backing store for the tile grid is a collection of TileEntity instances which is not typically resized at runtime.
let grid;

// Contains all the static information about TileEntities by ID.
let TILE_ENTITY_DEFINITIONS;

// Returns the definition belonging to a TileEntity
export const getTileEntityDefinition = (tent) => TILE_ENTITY_DEFINITIONS[tent.id];

/*
 * Returns a promise that resolves when the subsystem is initialized.
 * Takes a promise that resolves to a value taken from TileEntityDefinitions.json.
 * The JSON file is acquired using different means between client and server,
 * so it must be provided by the caller.
 */
export const init = tileDefPromise => tileDefPromise.then(tileDef =>
{
	TILE_ENTITY_DEFINITIONS = tileDef;
	grid = new Array(TILE_GRID_MAX_TILES).fill(null).map(() => new TileEntity({ id: 1 }));
	LOG.log("TileGrid initialized.");
	return Promise.resolve();
});

// Exports an empty visibility mask which is accepted by exportGrid.
export const generateEmptyVisibilityMask = () => Array(TILE_GRID_MAX_TILES).fill(false);

/*
 * Exports an array of TileEntity instances for serialization or manipulation.
 * Accepts an existingGrid by reference, which is used instead of instantiating a new grid
 * if provided. This is preferred as recreating the array is expensive.
 * Optionally accepts a visibilityMask, which is a grid of booleans referring to whether that TileEntity should be
 * visible. Invisible TileEntities are replaced with the "Unspecified" tile during export.
 */
export const exportGrid = (existingGrid, visibilityMask) =>
{
	if(existingGrid && existingGrid.length !== grid.length)
	{
		LOG.error("Cannot accept existingGrid of incorrect size! Expected", grid.length, "Got", existingGrid.length);
	}
	const retVal = existingGrid || [...grid];
	if(visibilityMask && visibilityMask.length  !== grid.length)
	{
		LOG.error("Cannot accept visibilityMask of incorrect size! Expected", grid.length, "Got", visibilityMask.length);
	}
	for(let i = 0; i < retVal.length; i++)
	{
		if(visibilityMask && !visibilityMask[i])
		{
			retVal[i] = TileEntity.unspecified();
		}
		else
		{
			retVal[i] = grid[i];
		}
	}
	return retVal;
};

// Conversion functions for going between the TileEntity array and GridPos.
export const getIndexFromGridPos = (gridPos) => gridPos.y * TILE_GRID_SIZE.x + gridPos.x;
export const getGridPosFromIndex = (index) =>
{
	const y = Math.floor(index / TILE_GRID_SIZE.x);
	const x = index - y * TILE_GRID_SIZE.x;
	return new GridPos(x, y);
};

/*
 * Imports an array of TileEntity instances in to the current TileEntity grid.
 * Will not apply changes if a TileEntity refers to the "Unspecified" tile.
 * Also accepts an callback function for when a change to a TileEntity is detected.
 * The callback is called with a GridPos instance and the TileEntity.
 */
export const importGrid = (tileGridData, updateTileCallback) =>
{
	if(tileGridData.length !== grid.length)
	{
		LOG.error("Cannot import tileGridData of incorrect size! Expected", grid.length, "Got", tileGridData.length);
	}
	tileGridData.forEach((tent, index) =>
	{
		if(tent.id !== 0 && grid[index].id !== tent.id)
		{
			grid[index].init(tent);
			if(updateTileCallback){updateTileCallback(getGridPosFromIndex(index), tent);}
		}
	});
};

// Returns a TileEntity given it's GridPos.
export const getTileEntity = (gridPos) => grid[getIndexFromGridPos(gridPos)];

/*
 * Creates a TileEntity at the provided position by configurator object.
 * Overwrites any preexisting TileEntities at that location.
 */
export const createTileEntity = (gridPos, ...args) => getTileEntity(gridPos).init(...args);

/*
 * Given a visibility mask, and an entity, updates the visibilityMask so that
 * grid positions in sight range are now visible.
 */
export const applySightRadiusToVisibilityMask = (visibilityMask, ent) =>
{
	const rad = Math.ceil(ent.sightRadius / SIM.GRID_SIZE);
	const startPos = GridPos.fromWorldPos(ent);
	for(let gridPos of generateGridPositionsInRange(startPos, rad))
	{
		visibilityMask[getIndexFromGridPos(gridPos)] = true;
	}
};

/*
 * Generator function that yields each grid position in a rasterized circle around the provided
 * gridPosition, within a specified gridRadius.
 */
export function* generateGridPositionsInRange(gridPos, gridRadius)
{
	const retVal = new GridPos();
	const rad2 = gridRadius * gridRadius;
	for(let y = -gridRadius; y <= gridRadius; y++)
	{
		const totalY = gridPos.y + y;
		if(totalY >= 0 && totalY < TILE_GRID_SIZE.y)
		{
			for(let x = -gridRadius; x <= gridRadius; x++)
			{
				const totalX = x + gridPos.x;
				if(totalX >= 0 && totalX < TILE_GRID_SIZE.x)
				{
					const dist2 = x * x + y * y;
					if(dist2 <= rad2)
					{
						retVal.x = totalX;
						retVal.y = totalY;
						yield retVal;
					}
				}
			}
		}
	}
}

// As generateGridPositionsInRange, but returns [GridPos, TileEntity].
export function* generateTileEntitiesInRange(gridPos, gridRadius)
{
	for(const _gridPos of generateGridPositionsInRange(gridPos, gridRadius))
	{
		yield [_gridPos, getTileEntity(_gridPos)];
	}
}

/*
 * Generator function that yields each TileEntity that is considered active.
 * Yielded values are a pair of [TileEntity, GridPos].
 */
export function* generateActiveTileEntities()
{
	for(let i = 0; i < grid.length; i++)
	{
		const tent = grid[i];
		if(tent.active){yield [tent, getGridPosFromIndex(i)];}
	}
}