// EventDTO is a serializable class intended to be sent to clients.
export default class EventDTO
{
	constructor(eventType, ...data)
	{
		Object.assign(this, ...data);
		this.type = eventType;
	}
}