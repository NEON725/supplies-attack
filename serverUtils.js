import { dirname as pathDirname } from "path";
import { fileURLToPath } from "url";
import { trace, warn } from "./common/logging.js";

// Some hackery to get the absolute path of the server root.
let __dirname;
export const dirname = () =>
{
	if(!__dirname)
	{
		__dirname = pathDirname(fileURLToPath(import.meta.url));
		trace(`Initialized project root at: ${__dirname}`);
	}
	return __dirname;
};

// Responds to a request with a provided error text and HTTP code.
export const errorResponse = (response, errorText, code) =>
{
	warn(errorText);
	response.writeHead(code || 500);
	response.end(errorText);
};

// Responds to a request with an object, which is serialized as a JSON string.
export const objectResponse = (response, retVal) =>
{
	const bodyString = JSON.stringify(retVal);
	trace(`Client response: ${bodyString.substring(0, 50)}...`);
	response.writeHead(200);
	response.end(bodyString);
};